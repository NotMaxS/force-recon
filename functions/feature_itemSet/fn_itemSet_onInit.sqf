/*
	FR_fnc_itemSet_onInit
    Locality: Local Effect
	Author: erem2k

    Initializes item set reference table (FR_itemSet_itemToSets) on mission start and attaches persistent event handlers.

    Reference table is a hash map of {String: Array{ Pair{String, Int} }}
        - Key corresponds to a classname of item of interest.
        - Value is an array of pairs
            * First member of a pair is item set name item in the key is part of.
            * Second member of pair is internal index of this item in a set.

    Reference table is intended to greatly speed up lookup of relationship between given inventory item classname and item sets it can be part of.

	Parameters:
        None

    Returns:
        None
*/

// Client only
if (!hasInterface) exitWith {};

// Populate reference table
private _itemToSetsTable = createHashMap;
private _itemSetNames = ["ItemSet"] call TEX_fnc_cfg_getConstantClasses;

{
    private _itemSetName = _x;
    private _itemsInSet = ["ItemSet", _itemSetName, "items"] call TEX_fnc_cfg_getConstant;

    if (isNil "_itemsInSet" || count _itemsInSet <= 0) then {

        [
            "warning", 
            format["[FR_fnc_itemSet_inInit] Item set %1 has no items defined!", _itemSetName]
        ] call XPT_fnc_log;

        continue; 
    };

    {
        private _itemName = _x;

        private _setsRelatedToItem = _itemToSetsTable getOrDefault [_itemName, [], true];
        _setsRelatedToItem pushBack [_itemSetName, _forEachIndex];

        // Prepares a table of { ItemSetName: [[ItemClassname, ItemIndexInSet], ...] }
        _itemToSetsTable set [_itemName, _setsRelatedToItem];

    } foreach _itemsInSet;

} foreach _itemSetNames;

// Finally, submit item reference table into missionNamespace
missionNamespace setVariable ["FR_itemSet_itemToSets", _itemToSetsTable];

// Attach persistent EHs
// Players can have items stripped off them while unconscious or captured, so we force repopulating table of carried items
[
    "FR_itemSet_eventItemSetUpdated", 
    {
        params["_player", "_carriedItemTable"];

        [_player, _carriedItemTable] call FR_fnc_itemSet_submitSetUpdate;
    }
] call CBA_fnc_addEventHandler;

[
    "ace_captiveStatusChanged", 
    {
        params ["_unit", "_isCaptive", "_reason"];

        // Global event, so requires checking if it's our player
        private _localPlayer = [] call ace_common_fnc_player;

        if (isNil "_localPlayer" || isNull _localPlayer) exitWith {};

        if (_unit == _localPlayer && _isCaptive) then {
            [_unit] call FR_fnc_itemSet_onPlayerLoadoutChanged;
        };
    }
] call CBA_fnc_addEventHandler;

[
    "ace_medical_WakeUp",
    {
        params ["_unit"];

        if (isNil "_unit" || isNull _unit) exitWith {};
        
        [_unit] call FR_fnc_itemSet_onPlayerLoadoutChanged;       
    }
] call CBA_fnc_addEventHandler;
