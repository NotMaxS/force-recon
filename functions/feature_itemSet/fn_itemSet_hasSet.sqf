/*
	FR_fnc_itemSet_hasSet
    Locality: Local Effect
	Author: erem2k

	Checks if unit has all items required to complete the item set.

	Parameters:
		0 - Object, unit
			Can be non-local, item registry for unit is shared across all clients
		
		1 - String, item set alias
			Must correspond to item set classname defined in CfgTEX >> MissionConstants >> ItemSet

	Returns:
		Boolean, whether unit has all items required to complete the set.
*/

params ["_unit", "_itemSetName"];

if (isNil "_unit" || isNull _unit) exitWith {};

private _unitItems = _unit getVariable "FR_itemSet_carriedItems";
if (isNil "_unitItems") exitWith {};

private _unitItemSetState = _unitItems getOrDefault [_itemSetName, []];

if (count _unitItemSetState == 0) exitWith {};

private _missingItemIndex = _unitItemSetState findIf { _x > 0 };

// Missing item index will be negative if all items in a set are accounted for
_missingItemIndex >= 0