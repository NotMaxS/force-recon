/*
	FR_fnc_itemSet_findSetCarrier
    Locality: Local Effect
	Author: erem2k

	Checks if any units in the list have all items required to complete requested set.

	Parameters:
		0 - Array of Object, unit list
			Typically passed from the trigger, units can be non-local
		
		1 - String, item set alias
			Must correspond to item set classname defined in CfgTEX >> MissionConstants >> ItemSet

	Returns:
		Integer, index of first unit in the list that has the entire set, or -1 if none found
*/

params[["_units", []], ["_itemSetName", ""]];

if (count _units <= 0) exitWith {};

if (count _itemSetName <= 0) exitWith {};

_units findIf { [_x, _itemSetName] call FR_fnc_itemSet_hasSet; }