/*
	FR_fnc_itemSet_onPlayerLoadoutChanged
    Locality: Global Effect
	Author: erem2k

    Parses items carried by the unit, builds item set table (FR_itemSet_carriedItems) and submits assign job to the unit.
    Carried item set table is required to achieve quick on-demand lookup of whether the unit has all the items to complete the given set.

	Parameters:
	    0 - Object, player unit
            Received from event handler or event script
    
    Returns:
        None
*/

params ["_player"];

// Set up player table structure
private _itemSets = ["ItemSet"] call TEX_fnc_cfg_getConstantClasses;
private _carriedItemTable = createHashMap;

{
    private _itemSetName = _x;
    private _itemsInSet = ["ItemSet", _itemSetName, "items"] call TEX_fnc_cfg_getConstant;

    if (isNil "_itemsInSet") then { 
        continue; 
    };

    private _itemSetState = [];
    {
        _itemSetState pushBack 0;
    } forEach _itemsInSet;

    // In the end, creates a map of { ItemSetName : [AmountOfHeldItemA, AmountOfHeldItemB, ...] }
    _carriedItemTable set [_itemSetName, _itemSetState];

} foreach _itemSets;

// Do a pass over all player items to populate player table
private _playerItems = items _player + magazines _player;
private _itemToSetsTable = missionNamespace getVariable ["FR_itemSet_itemToSets", createHashMap];

{
    private _itemName = _x;
    private _setsRelatedToItem = _itemToSetsTable getOrDefault [_itemName, []];

    if (count _setsRelatedToItem <= 0) then { continue; };

    {
        private _setName = _x select 0;
        private _itemIndexInSet = _x select 1;

        // Passing over all matching sets and incrementing amount of held item
        private _setItemState = _carriedItemTable get _setName;
        _setItemState set [_itemIndexInSet, (_setItemState select _itemIndexInSet) + 1];

    } forEach _setsRelatedToItem;

} forEach _playerItems;

// Finally, submit carried item table update for player unit
["FR_itemSet_eventItemSetUpdated", [_player, _carriedItemTable]] call CBA_fnc_localEvent;