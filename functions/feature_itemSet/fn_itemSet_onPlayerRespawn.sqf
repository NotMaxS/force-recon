/*
	FR_fnc_itemSet_onPlayerRespawn
    Locality: Global Effect
	Author: erem2k

    Attaches handlers required for itemSet system to player on respawn.
    Additionally, sets up a table of item set items already carried by the player.

	Parameters:
	    0 - Object, spawned player unit
            Received from onPlayerRespawn event script
    
    Returns:
        None
*/

params ["_player"];

// This will populate item set state on startup, and keep track of any further loadout state changes.
// It's less performant than having put/take event handlers, but in the end it's miles easier to maintain.
[
    "loadout", 
    {
        [[] call ace_common_fnc_player] call FR_fnc_itemSet_onPlayerLoadoutChanged; 
    }
] call CBA_fnc_addPlayerEventHandler;
