/*
	FR_fnc_itemSet_diag
    Locality: Local Effect
	Author: erem2k

	Outputs debug information about itemSet feature.
    Optionally can also attach per frame monitor of local variable state.

	Parameters:
		0 - Boolean, whether to show variable monitor hint.
			Passing False when monitor is being shown will disable it.

	Returns:
		String, current state of itemSet variables on this client.
*/

params [["_attachMonitor", false]];

// Fetch and stringify local variable state
private _fn_varsToString = {

	private _globalVarState = str (missionNamespace getVariable ["FR_itemSet_itemToSets", createHashMap]);
	private _localVarState = str (player getVariable ["FR_itemSet_carriedItems", createHashMap]);

	format ["Global:\n%1\n\nLocal:\n%2", _globalVarState, _localVarState];
};

// Attach/remove per frame handler
private _existingDiagHandler = missionNamespace getVariable ["FR_itemSet_diagPFH", nil];

if (isNil "_existingDiagHandler") then {

	if (_attachMonitor) then {
		
		private _diagHandler = [{ hintSilent (format ["DIAG itemSet\n%1", call (_this select 0)]) }, 2, _fn_varsToString] call CBA_fnc_addPerFrameHandler;
		missionNamespace setVariable ["FR_itemSet_diagPFH", _diagHandler];
	};

} else {

	if (!_attachMonitor) then {
		[_existingDiagHandler] call CBA_fnc_removePerFrameHandler;
	};
};

// Finally, dump the varstate string
private _varState = call _fn_varsToString;
diag_log format ["[FR_fnc_itemSet_diag] %1", _varState];

_varState