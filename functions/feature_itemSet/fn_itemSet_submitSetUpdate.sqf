/*
	FR_fnc_itemSet_submitSetUpdate
    Locality: Global Effect
	Author: erem2k

    Submits an update of carried item table for the unit over the network.
        Local state is updated immediately, network state is updated periodically if any local updates are available.

    Why not just setVariable the state immediately over the network?
    Player loadout changes events are emitted very frequently by CBA, so we want to throttle potential network spam by making data transmissions periodic.

	Parameters:
	    0 - Object, player unit
            Received from event handler, their loadout state was changed.
        
        1 - Hash map, item set table
            Built by FR_fnc_itemSet_onPlayerLoadoutChanged, contains latest item set state.
    
    Returns:
        None
*/

params ["_unit", "_carriedItemTable"];

_unit setVariable ["FR_itemSet_carriedItems", _carriedItemTable];

// Don't schedule sync if update is pending
private _isUpdatePending = missionNamespace getVariable ["FR_itemSet_updatePending", false];
if (_isUpdatePending) exitWith {};

// Schedule synchronization
[_unit] spawn {
    sleep (["ItemSet", "syncDelay"] call TEX_fnc_cfg_getConstant);
    
    missionNamespace setVariable ["FR_itemSet_updatePending", false];
    private _updatedUnit = _this select 0;

    // Sync variable over network, picking up last changes from local state
    _updatedUnit setVariable["FR_itemSet_carriedItems", (_updatedUnit getVariable "FR_itemSet_carriedItems"), true];
};

missionNamespace setVariable ["FR_itemSet_updatePending", true];