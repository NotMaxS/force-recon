/*
	FR_fnc_itemSet_removeSet
    Locality: Global Effect
	Author: erem2k

	Removes one of each items from passed item set from unit's inventory and updates carried items table.
	Checks if unit has all items in the set before removal, denies operation if not.

	Parameters:
	    0 - Object, player unit
			Can be non-local

		1 - String, item set name
			Must correspond to item set classname defined in CfgTEX >> MissionConstants >> ItemSet
	
	Returns:
		Bool, whether item set was successfully removed
*/

params [["_unit", objNull], ["_itemSetName", ""]];

// Sanity checks
if (isNil "_unit" || isNull _unit) exitWith {
	["error", "[FR_fnc_itemSet_removeSet] Invalid unit passed!"] call XPT_fnc_log;
	false;
};

if (count _itemSetName <= 0) exitWith {
	["error", "[FR_fnc_itemSet_removeSet] Invalid item set name passed!"] call XPT_fnc_log;
	false;
};

// Check if unit has the set first
if (!([_unit, _itemSetName] call FR_fnc_itemSet_hasSet)) exitWith {
	false;
};

// Go over all items, update sets containing them one by one, and dump them from inventory
private _itemsInRequestedSet = ["ItemSet", _itemSetName, "items"] call TEX_fnc_cfg_getConstant;

private _itemToSetsTable = missionNamespace getVariable ["FR_itemSet_itemToSets", createHashMap];
private _unitCarriedItems = _unit getVariable ["FR_itemSet_carriedItems", createHashMap];

{
	private _itemName = _x;

	// Update the carried item table first, then do the actual item removal from the inventory
	private _containingSetInfos = _itemToSetsTable getOrDefault [_itemName, []];

	{
		private _containingSetName = _x select 0;
		private _containingSetItemIndex = _x select 1;

		private _containingSetState = _unitCarriedItems getOrDefault [_containingSetName, []];
		if (count _containingSetState <= 0) then { continue; };

		// Update item count in set state
		private _updatedItemState = _containingSetState select _containingSetItemIndex;
		_updatedItemState = (_updatedItemState - 1) max 0;

		_containingSetState set [_containingSetItemIndex, _updatedItemState];

		// Update the parent carried items table
		_unitCarriedItems set [_containingSetName, _containingSetState];

	} forEach _containingSetInfos;

	_unit removeItem _itemName;

} foreach _itemsInRequestedSet;

// Finally, update carried item table
["FR_itemSet_eventItemSetUpdated", [_unit, _unitCarriedItems]] call CBA_fnc_localEvent;

true