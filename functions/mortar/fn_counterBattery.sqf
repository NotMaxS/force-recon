
params["_arty", "_firedPlayer"];    

private _artySplashPosition = getPos _arty;
private _randomDelay = random [90,135,180];  

    /* _randomTime = random [1,2,3]; */

["ETA to counter battery: 1.5 minutes to 3 minutes."] remoteExec ["hint", _firedPlayer];

private _randomSplashPositions = [locationNull, locationNull, locationNull, locationNull];

{
    private _randomPos = [[[position _arty,100]],[[position _arty,50]]] call BIS_fnc_randomPos;
    _randomSplashPositions set [_forEachIndex, _randomPos];
} forEach _randomSplashPositions;

[
    {
        _this spawn 
        {
            params ["_artyPos", "_randomPosArray"];
            
            "HelicopterExploBig" createVehicle _artyPos;
            {
                "HelicopterExploBig" createVehicle _x;
                sleep (random [1,3,5]);
            } forEach _randomPosArray;
        };
    }, 
    [_artySplashPosition, _randomSplashPositions], 
    _randomDelay
] call CBA_fnc_waitAndExecute;