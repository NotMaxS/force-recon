private _fnc_flattenTerrain = 
{ 
 params ["_start", "_a", "_b", "_h"]; 
 private _newPositions = []; 
 
 for "_xStep" from 0 to _a do 
 { 
  for "_yStep" from 0 to _b do 
  { 
   private _newHeight = _start vectorAdd [_xStep, _yStep, 0]; 
   _newHeight set [2, _h]; 
   _newPositions pushBack _newHeight; 
  }; 
 }; 
 
 _newPositions; 
}; 

/*
    _fnc_flattenTerrain [_startingPosition, _xStep, _yStep, _desiredTerrainHeight];
*/

// get all the unnamed triggers in a mission:
_allTriggers = [];
if (is3DEN) then {
    _allTriggers = get3DENLayerEntities 313;

    //systemChat format ["_allTriggers = %1", _allTriggers];
    {
        _posASL = ATLToASL ((_x get3DENAttribute "position") # 0);
        //systemChat format ["_posASL: %1", _posASL];
        ((_x get3DENAttribute "size3") # 0) params ["_a", "_b", "_c"];
        //(triggerArea _x) params ["_a", "_b", "_angle", "_isRectangle", "_c"];
        _desiredTerrainHeight = (_posASL # 2) - _c;
        _startingPos = [(_posASL # 0) - _a, (_posASL # 1) - _b];
        _xLength = _a * 2;
        _yLength = _b * 2;
        _positionAndHeights = [_startingPos, _xLength, _yLength, _desiredTerrainHeight] call _fnc_flattenTerrain;
        //systemChat format ["%1 deform formatted: %2", _x, _positionAndHeights];
        setTerrainHeight [_positionAndHeights, false];
        //systemChat format ["%1 deform applied.", _x];
    } forEach _allTriggers;
} else {
    //_allTriggers = allMissionObjects "EmptyDetector";
    _allTriggers = (getMissionLayerEntities "terrainDeform") select 0;

    //systemChat format ["_allTriggers = %1", _allTriggers];
    {
        _posASL = getPosASL _x;
        (triggerArea _x) params ["_a", "_b", "_angle", "_isRectangle", "_c"];
        _desiredTerrainHeight = (_posASL # 2) - _c;
        _startingPos = [(_posASL # 0) - _a, (_posASL # 1) - _b];
        _xLength = _a * 2;
        _yLength = _b * 2;
        //systemChat format ["%1 deform formatted.", _x];
        _positionAndHeights = [_startingPos, _xLength, _yLength, _desiredTerrainHeight] call _fnc_flattenTerrain;
        setTerrainHeight [_positionAndHeights, false];
        //systemChat format ["%1 deform applied.", _x];
    } forEach _allTriggers;
};
/*
_allTriggers = [];
{
    if (typeOf _x isEqualTo "EmptyDetector") then 
    {
        _allTriggers pushBack _x;
        systemChat format ["%1 added to deform list.", _x];
    } forEach vehicles;
};
*/


/*
private _desiredTerrainHeight = 45; 
private _positionsAndHeights = [[908,384], 13*8, 4, _desiredTerrainHeight] call _fnc_flattenTerrain; 
setTerrainHeight [_positionsAndHeights, false];
private _positionsAndHeights = [[908,384], 4, 4*8, _desiredTerrainHeight] call _fnc_flattenTerrain; 
setTerrainHeight [_positionsAndHeights, false];
private _positionsAndHeights = [[892,408], 3*8, 4, _desiredTerrainHeight] call _fnc_flattenTerrain; 
setTerrainHeight [_positionsAndHeights, false];
private _positionsAndHeights = [[892,408], 4, 3*8, _desiredTerrainHeight] call _fnc_flattenTerrain; 
setTerrainHeight [_positionsAndHeights, false];
private _positionsAndHeights = [[860,424], 5*8, 4, _desiredTerrainHeight] call _fnc_flattenTerrain; 
setTerrainHeight [_positionsAndHeights, false];
*/