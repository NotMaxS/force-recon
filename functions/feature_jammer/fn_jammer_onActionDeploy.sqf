/*
	FR_fnc_jammer_onActionDeploy
    Locality: Global Effect
	Author: erem2k

    Strips jammer item set from the unit, spawns jammer object and attempts to get the unit to ACE carry it.
        Also removes the zone marker where deployment is taking place.

	Parameters:
	    0 - Object, unit deploying the jammer.
		    Carried item table for this unit will be modified, and it will start carrying the jammer.
    
    Returns:
        None
*/

params ["_deployingUnit"];

if (isNil "_deployingUnit") exitWith {
    ["error", "[FR_fnc_jammer_onActionDeploy] No _deployingUnit specified, aborting"] call XPT_fnc_log;
};

if (!([_deployingUnit, "JammerObjective"] call FR_fnc_itemSet_removeSet)) exitWith {
    ["error", "[FR_fnc_jammer_onActionDeploy] Failed to remove JammerObjective items from unit, aborting"] call XPT_fnc_log;
};

private _jammerClassname = ["Jammer", "worldObject"] call TEX_fnc_cfg_getConstant;
private _jammerObject = _jammerClassname createVehicle (getPosATL _deployingUnit);

// Configure the object
_jammerObject allowDamage false;

/// Update locally first - it's needed to be carryable for the next command
[_jammerObject, true, nil, nil, true] call ace_dragging_fnc_setCarryable;
[_jammerObject, true, nil, nil, true] remoteExec ["ace_dragging_fnc_setCarryable", -clientOwner, true];

// Force deploying unit to carry the jammer and emit appropriate events for result handling
if ([_deployingUnit, _jammerObject] call ace_dragging_fnc_canCarry) then {
    
    [_deployingUnit, _jammerObject] call ace_dragging_fnc_startCarry;
    ["FR_jammer_deployStarted", [_jammerObject, _deployingUnit]] call CBA_fnc_localEvent;

} else {
    ["FR_jammer_deployCompleted", [_jammerObject]] call CBA_fnc_globalEventJIP;
};

// Finally, delete the marker unit is deploying the jammer at
// This will also take care of denying further deploy action availability for this area
private _deployZones = missionNamespace getVariable["FR_jammer_objectiveZones", []];
private _currentZones = _deployZones select { _deployingUnit inArea _x; };
{
    deleteMarker _x;
} forEach _currentZones;

missionNamespace setVariable["FR_jammer_objectiveZones", _deployZones - _currentZones];