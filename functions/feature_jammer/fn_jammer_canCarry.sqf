/*
	FR_fnc_jammer_canCarry
    Locality: Local Effect
	Author: erem2k

    Checks if given unit can carry objects using ACE dragging.
        Overall logic is liften from ACE repo, sans the check against carried object.
        Because why would you ever need to check if unit can carry *anything*, right?

	Parameters:
        0 - Object, player unit.
            Can be non-local.
    
    Returns:
        Boolean, whether player unit can carry objects.
*/

params["_playerUnit"];

if (isNull _playerUnit) exitWith {false};

// Must be alive
if (!alive _playerUnit) exitWith {false};

// Must be conscious
if (_playerUnit getVariable ["ACE_isUnconscious", false]) exitWith {false};

// Their legs must be OK
if ((_playerUnit getHitPointDamage "HitLegs") >= 0.5) exitWith {false};

// Must be not in a vehicle
if (vehicle _playerUnit != _playerUnit) exitWith {false};

// Must be not underwater
if (surfaceIsWater getPos _playerUnit) exitWith {false};

// Must not be carrying/dragging something else
if (_playerUnit getVariable ["ace_dragging_isCarrying", false]) exitWith {false};

if (_playerUnit getVariable ["ace_dragging_isDragging", false]) exitWith {false};

true