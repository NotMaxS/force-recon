/*
	FR_fnc_jammer_canDeploy
    Locality: Local Effect
	Author: erem2k

    Checks if given unit can deploy the jammer.
        Typically used inside action conditionShow code.
    
    Jammer can be deployed if:
        - Player has all the items required for it's deployment
        - Player is in the correct zone
        - Player is alive
        - Player is conscious
        - Player is not in a vehicle
        - Player is standing still

	Parameters:
        0 - Object, player unit.
            Can be non-local.
    
    Returns:
        Boolean, whether player unit can deploy the jammer
*/

params["_playerUnit"];

// Why this ghetto-ass setup and not a set of booleans?

// We want to minimize processing overhead as this check will be ran every frame
//  * Collection of booleans means all expressions will be evaluated before final result is returned
//  * Stacking condition evaluations in the end will make it unreadable
// So, we define a set of functions which will encapsulate every check and will be evaluated in order from left to right
//  where returned first "false" will end check prematurely, thus saving us some processing time 

private _fn_isInObjectiveZone = {

    private _unit = _this select 0;
    private _objZOnes = missionNamespace getVariable ["FR_jammer_objectiveZones", []];

    (_objZones findIf { _unit inArea _x }) >= 0
};

private _fn_hasAllItems = { 

    private _unit = _this select 0;
    [_unit, "JammerObjective"] call FR_fnc_itemSet_hasSet;
};

private _fn_canCarry = { // Covers unit being alive, conscious, out of vehicle and being uninjured

    private _unit = _this select 0;
    [_unit] call FR_fnc_jammer_canCarry;
};

private _fn_isStandingStill = { // Speed can be negative!
    
    private _unit = _this select 0;
    private _unitSpeed = speed _unit;
    private _canBeDeployed = false;

    if (_unitSpeed > 0.0) then {
       _canBeDeployed = _unitSpeed < 1.0;
    } else {
        _canBeDeployed = _unitSpeed > (-1.0);
    };

    _canBeDeployed
};

[_playerUnit] call _fn_isInObjectiveZone && [_playerUnit] call _fn_hasAllItems && [_playerUnit] call _fn_canCarry && [_playerUnit] call _fn_isStandingStill