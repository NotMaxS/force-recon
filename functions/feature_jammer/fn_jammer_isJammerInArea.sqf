/*
	FR_fnc_jammer_isJammerInArea
    Locality: Local Effect
	Author: erem2k

    Checks if jammer entity is in provided object list

	Parameters:
	    0 - Array of Object, object list
		    Typically passed from the trigger
    
    Returns:
        Boolean, whether jammer entity is in provided object list
*/

params["_objectList"];

private _jammerClassname = ["Jammer", "worldObject"] call TEX_fnc_cfg_getConstant;

_objectList findIf { _x isKindOf _jammerClassname} >= 0