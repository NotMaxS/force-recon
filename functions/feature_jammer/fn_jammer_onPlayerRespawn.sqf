/*
	FR_fnc_jammer_onPlayerRespawn
    Locality: Local Effect
	Author: erem2k

    Attaches jammer deployment action on player respawn.
    
	Parameters:
        None
    
    Returns:
        None
*/

params ["_playerUnit"];

missionNamespace setVariable ["FR_jammer_deploySoundId", -1];
private _actionDuration = ["Jammer", "Action", "sfxDuration"] call TEX_fnc_cfg_getConstant;


[
    _playerUnit,
    "Deploy Jammer",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdaction_connect_ca.paa",
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdaction_connect_ca.paa",
    "[_target] call FR_fnc_jammer_canDeploy", // conditionShow 
    "[_target] call FR_fnc_jammer_canDeploy", // conditionProgress
    {
        // codeStart
        private _soundId = playSound3D [getMissionPath (["Jammer", "Action", "sfxPath"] call TEX_fnc_cfg_getConstant), _caller];

        missionNamespace setVariable ["FR_jammer_deploySoundId", _soundId];
    },
    {},
    {
        // codeCompleted
        [_caller] call FR_fnc_jammer_onActionDeploy;
        missionNamespace setVariable ["FR_jammer_deploySoundId", -1];
    },
    {
        // codeInterrupted
        private _soundId = missionNamespace getVariable ["FR_jammer_deploySoundId", -1];

        if (_soundId < 0) exitWith {};

        stopSound _soundId;
        missionNamespace setVariable ["FR_jammer_deploySoundId", -1];
    },
    nil,
    _actionDuration,
    nil,
    false
] call BIS_fnc_holdActionAdd;