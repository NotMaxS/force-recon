/*
	FR_fnc_jammer_onDeployStarted
    Locality: Global Effect
	Author: erem2k

    Awaits for player to drop the jammer onto the ground to start post-deployment handling logic.

	Parameters:
	    0 - Object, jammer world object.
            Carried or dragged by player.
        
        1 - Object, deploying player unit.
    
    Returns:
        None
*/

params["_jammer", "_deployingUnit"];

private _fn_awaitJammerPlacement = {

    params ["_deployingUnit", "_carriedJammer"];

    [
        {
            private _unit = _this select 0;
            
            private _isCarrying = _unit getVariable ["ace_dragging_isCarrying", false];
            private _isDragging = _unit getVariable ["ace_dragging_isDragging", false];

            !(_isCarrying) && !(_isDragging)
        }, 
        {
            private _carriedJammer = _this select 1;
            ["FR_jammer_deployCompleted", [_carriedJammer]] call CBA_fnc_globalEventJIP;
        }, 
        [_deployingUnit, _carriedJammer]
    ] call CBA_fnc_waitUntilAndExecute;
};

// Just in case vars don't get set this frame
[_fn_awaitJammerPlacement, [_deployingUnit, _jammer]] call CBA_fnc_execNextFrame;