/*
	FR_fnc_jammer_onDeployCompleted
    Locality: Global Effect
	Author: erem2k

    Restricts jammer from being carried or dragged, and starts the playback of ambient SFX

	Parameters:
	    0 - Object, jammer world object.
    
    Returns:
        None
*/

params["_jammer"];

// Disable carrying/dragging
[_jammer, false, nil, nil, true] call ace_dragging_fnc_setCarryable;
[_jammer, false, nil, nil, true] call ace_dragging_fnc_setDraggable;

// And have server only run global commands
if (isServer) then {
    
    // Disable sim to make it immovable; delay is here to allow it to fall down on the ground
    [
        {
            _this enableSimulationGlobal false;
        }, 
        _jammer, 
        5
    ] call CBA_fnc_waitAndExecute;
    
    // Start playback of ambient SFX
    createSoundSource [["Jammer", "Ambient", "sfxType"] call TEX_fnc_cfg_getConstant, position _jammer, [], 0];
};
