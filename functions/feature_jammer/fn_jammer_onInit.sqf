/*
	FR_fnc_jammer_onInit
    Locality: Global Effect
	Author: erem2k

    Attaches handlers to jammer deployment events.
        On server, also builds a list of active jammer objective zones based on markers on init.

	Parameters:
        None

    Returns:
        None
*/

// Attach persistent EHs to jammer deployment start/completion
// Note that this is NOT the same as deployment action completion - for jammer to be deployed, player has to set it down in the area
[
    "FR_jammer_deployStarted", 
    {
        _this call FR_fnc_jammer_onDeployStarted;
    }
] call CBA_fnc_addEventHandler;

[
    "FR_jammer_deployCompleted", 
    { 
        _this call FR_fnc_jammer_onDeployCompleted; 
    }
] call CBA_fnc_addEventHandler;

// Server also maintains a list of objective markers
if (isServer) then {

    private _jammerZoneNameTemplate = ["Jammer", "objMarkerTemplate"] call TEX_fnc_cfg_getConstant;
    private _jammerZoneMarkers = allMapMarkers select { _jammerZoneNameTemplate in _x};

    missionNamespace setVariable ["FR_jammer_objectiveZones", _jammerZoneMarkers, true];
};