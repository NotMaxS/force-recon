/*
	FR_fnc_markers_bulkMoveLayer
	Author: erem2k
	Bulk moves markers from source to target layer

	Parameters:
	0 - Number, source layer channel ID
		ID of the channel to move the markers from, they will also be wiped to avoid duplication
	1 - Number, target layer channel ID
        ID of the channel to move the markers to
	
	Returns: None
*/

params["_sourceLayerId", "_targetLayerId"];

// Server-only machine execution
if (!isServer) exitWith {};

// Serialize all user markers from the source layer into the string
private _sourceMarkerArray = [];
private _sourceMarkerString = "";
{
	if (_x find "_USER_DEFINED #" != -1 AND markerChannel _x == _sourceLayerId) then {
		if (markerShape _x == 'polyline') then {
			_sourceMarkerArray = _sourceMarkerArray + [[_x, getMarkerPos _x, markerChannel _x, toLower(markerShape _x), getMarkerSize _x, getMarkerType _x, "", markerPolyline _x, getMarkerColor _x, _side]];
		}
		else {
			_sourceMarkerArray = _sourceMarkerArray + [[_x, getMarkerPos _x, markerChannel _x, toLower(markerShape _x), getMarkerSize _x, getMarkerType _x, markerText _x, [], getMarkerColor _x, _side]];
		};
	};
} forEach allMapMarkers;

_sourceMarkerString = str _sourceMarkerArray;

// Clear the source layer
{
	if (_x find "_USER_DEFINED #" != -1 AND markerChannel _x == _sourceLayerId) then {
		deleteMarker _x;
	};
} forEach allMapMarkers;

// Have markers created globally
[_targetLayerId, _sourceMarkerString] call FR_fnc_markers_bulkCreateLayer;
