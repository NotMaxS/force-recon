/*
	FR_fnc_amrkers_bulkCreateLayer
	Author: erem2k
	Deserealizes markers from the passed string and locally creates them on target layer

	Parameters:
	0 - Number, target layer channel ID
		ID of the channel where markers will be created
	1 - String, serialized marker array string
        
	Returns: None
*/
params["_targetLayerId", "_markerString"];

private _markerArray = parseSimpleArray _markerString;

private _markerNameFormat = "SR_MARKER_#%1";
private _markerIdCounter = 0;

{
	_x params [
		"_name",
		"_pos",
		"_channel",
		"_shape",
		"_size",
		"_type",
		"_txt",
		"_polyline",
		"_col",
		"_side"
	];

    // Figure out the name first, we want it to be unique
	private _resolvedName = format[
            _markerNameFormat
            , _markerIdCounter
    ];

	// Create marker and set it's parameters
    private _marker = createMarker [_resolvedName, _pos, _targetLayerId];

	_marker setMarkerShapeLocal _shape;
	_marker setMarkerSizeLocal _size;
	_marker setMarkerTypeLocal _type;

	if (toLower(_shape) == 'polyline') then {
		_marker setMarkerPolylineLocal _polyline;
	}
	else {
		_marker setMarkerTextLocal _txt;
	};

	// Global marker property commands also globally transmit previous local changes
    _marker setMarkerColor _col;

    // Don't forget the counter
    _markerIdCounter = _markerIdCounter + 1;

} forEach _markerArray;