/*
	FR_fnc_setupSpectator
	Author: erem2k
*/

params ["_unit", "_killer", "_respawn", "_respawnDelay"];

// Will make TFAR hearable in spectator mode if player is waiting for respawn, disable mode otherwise
if (_unit isKindOf "ace_spectator_virtual") then {
    _unit setVariable ["TFAR_forceSpectator", true];
}
else {
    _unit setVariable ["TFAR_forceSpectator", playerRespawnTime > 1];
};