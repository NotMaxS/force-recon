//Obtains number of tasks completed that use ["FR_MissionTaskCompletedEvent", ["TASK NAME", "TASK STATE"]] call CBA_fnc_serverEvent
//Determines a cfg debriefing class in accordance with the number
//Technically cannot go above 4, case number 5 is just in case as there are 5 tasks done using this method


private _sumofTasks = missionNamespace getVariable ["FR_mission_tasksCompleted", 0];

switch (_sumofTasks) do {
    case 5;
    case 4:
    {
        "youTotallyWon" call BIS_fnc_endMissionServer; 
    };
    case 3: 
    {
        "youPartiallyWon" call BIS_fnc_endMissionServer; 
    };
    case 2:
    {
        "youHadMinorLoss" call BIS_fnc_endMissionServer; 
    };
    case 1;
    case 0:
    {
        "youDidNotWin" call BIS_fnc_endMissionServer; 
    };
};

