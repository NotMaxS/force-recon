//Variables corresponding to each trigger that activate main tasks 
//5 exist in total, one is extra in case the HVT dies 
//4 are needed for players to win the mission 

params["_taskName", "_taskState"];

[_taskName, _taskState] call BIS_fnc_taskSetState;

if (_taskState != "SUCCEEDED") exitWith {};

private _completedTasks = missionNamespace getVariable ["FR_mission_tasksCompleted", 0];

missionNamespace setVariable ["FR_mission_tasksCompleted", _completedTasks + 1];