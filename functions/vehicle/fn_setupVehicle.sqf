// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", objNull, [objNull]],
	["_oldVeh", objNull, [objNull]],
	["_start", false, [true]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {

	
	// Drone and command pickup 
	case (toLower "B_T_Pickup_Comms_rf"): {

	//load inventory 	
	[_newVeh, "dronePickup"] call XPT_fnc_loadItemCargo;

	
	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};


	//mortar team offroad 
	case (toLower "B_GEN_Offroad_01_comms_F"): {

	//load inventory 	
	[_newVeh, "mortarPickup"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};

	//sniper team's ATV 
	case (toLower "B_Quadbike_01_F"): {

	//load inventory 	
	[_newVeh, "sniperATV"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};

	//broken M1 abrams to be repaired by players
	case (toLower "CUP_B_M1A1SA_TUSK_Woodland_US_Army"): {

	//load inventory 	
	[_newVeh, "brokenTank"] call XPT_fnc_loadItemCargo;

	//damage for parts
	_newVeh setHit ["hit_hull_point", 0.75]; 
	_newVeh setHit ["hit_engine_point", 1]; 
	_newVeh setHit ["hit_trackl_point", 1]; 
	_newVeh setHit ["hit_trackr_point", 1];
	_newVeh setHit ["hit_fuel_point", 1]; 
	_newVeh setHit ["hit_main_gun_point", 1]; 

	//remove thermals
	_newVeh disableTIEquipment true;

	//remove weapons from the gunner turret of the abrams
	[_newVeh,[ "CUP_Vcannon_M256_M1Abrams", [0]]] remoteExec ["removeWeaponTurret", _newVeh turretOwner [0]];
	[_newVeh,[ "CUP_Vhmg_M2_M1Abrams_TUSK_Coax", [0]]] remoteExec ["removeWeaponTurret", _newVeh turretOwner [0]];
	[_newVeh,[ "CUP_Vlmg_M240_M1Abrams_Coax", [0]]] remoteExec ["removeWeaponTurret", _newVeh turretOwner [0]];

	//remove M2 from turret of commander, leave his smoke launcher
	[_newVeh,[ "CUP_Vhmg_M2_M1Abrams_Commander2", [0,0]]] remoteExec ["removeWeaponTurret", _newVeh turretOwner [0]];

	//remove all but one magazine from the loader's m240 
	[_newVeh,[ "CUP_200Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M", [0,2]]] remoteExec ["removeMagazinesTurret", _newVeh turretOwner [0,2]];
	[_newVeh,[ "CUP_200Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M", [0,2]]] remoteExec ["addMagazineTurret", _newVeh turretOwner [0,2]];
	
	//give more smoke grenades to the smoke launcher of the Abrams
	[_newVeh,[ "SmokeLauncherMag", [0,0], 4]] remoteExec ["addMagazineTurret", _newVeh turretOwner [0,0]];

	//fuel will be set to 0 whenever players enter trigger area
	_newVeh setFuel 0; 

	//Enable datalink
	_newVeh setVehicleReportOwnPosition true;
	};

	//stolen opfor vehicle inventory 
	case (toLower "CUP_O_Kamaz_6396_fuel_RUS_M"): {

	//load inventory 	
	[_newVeh, "brokenTank"] call XPT_fnc_loadItemCargo;
	};

	case (toLower "CUP_O_Kamaz_6396_repair_RUS_M"): {

	//load inventory 	
	[_newVeh, "brokenTank"] call XPT_fnc_loadItemCargo;
	};

	//Prowlers   
	case (toLower "B_LSV_01_unarmed_F"): {

	//load inventory 	
	[_newVeh, "squadVehicle"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};


	//orangutan team pickup 
	case (toLower "B_G_Offroad_01_armor_base_lxWS"): {

	//load inventory 	
	[_newVeh, "squadVehicle"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};

	//extraction chopper 
	case (toLower "B_Heli_Transport_03_unarmed_F"): {

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	//Create extraction hold action on the helicopter 
	[     
			 _newVeh,              
			 "Extract",           
			 "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_Takeoff1_ca.paa",    
			 "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_Takeoff1_ca.paa",      
			 "player distance _target < 3 && side player == west",         
			 "player distance _target < 3 && side player == west",         
			 {},               
			 {},              
			 {taskmanExtract setDamage 1;},      
			 {},             
			 [],                 
			 8,                
			 0,                
			 true,              
			 false             
		] remoteExec ["BIS_fnc_holdActionAdd", 0, _newVeh];

	};

	//rearm pickup 
	case (toLower "B_T_Pickup_rf"): {

	//load inventory 	
	[_newVeh, "mortarPickup"] call XPT_fnc_loadItemCargo;

	//This is now a rearm vehicle
	[_newVeh] call ace_rearm_fnc_makeSource; 

	//Adds first complete commando mortar re-arm loadout
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;

	[_newVeh, "2Rnd_60mm_Mo_Flare_white_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "2Rnd_60mm_Mo_Smoke_white_RF"] call ace_rearm_fnc_addMagazineToSupply;

	//Adds second complete commando mortar re-arm loadout
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "4Rnd_60mm_Mo_shells_RF"] call ace_rearm_fnc_addMagazineToSupply;

	[_newVeh, "2Rnd_60mm_Mo_Flare_white_RF"] call ace_rearm_fnc_addMagazineToSupply;
	[_newVeh, "2Rnd_60mm_Mo_Smoke_white_RF"] call ace_rearm_fnc_addMagazineToSupply;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	};


};