/*
	FR_fnc_fpv_onVehicleInit
    Locality: Global Effect
	Author: erem2k

    Initializes RC-40 drone and overrides RF detonation behavior.

	Parameters:
	    0 - Object, RC-40 drone
    
    Returns:
        None
*/

params["_fpvVehicle"];

// Prevent double inits due to retarded class layout
if (_fpvVehicle getVariable ["FR_fpv_isInitialized", false]) exitWith {};

// Override the fuel coefficient to desired value
private _fuelCoefOverride = ["FPV", "fuelConsumptionCoef"] call TEX_fnc_cfg_getConstant;

if (local _fpvVehicle && !(isNil "_fuelCoefOverride")) then {
    _fpvVehicle setFuelConsumptionCoef _fuelCoefOverride;
};

// Smoke FPVs have their behavior overriden as special ammo carriers
private _fn_overrideKilledEH = {
    
    params["_uav"];

    _uav removeAllEventHandlers "Killed";

    _uav addEventHandler [
        "Killed", 
        {
            params ["_unit", "_killer", "_instigator", "_useEffects"];
            [_unit] call FR_fnc_fpv_onKilled;
        }
    ];
};

if (_fpvVehicle isKindOf "UAV_RC40_Base_SmokeWhite_RF") then {

    // Postpone removing config-supplied Killed EH until next frame
    // I'm unsure on sequencing between attaching config EHs and this callback, so better to be safe for MP test
    [_fn_overrideKilledEH, _fpvVehicle] call CBA_fnc_execNextFrame;

    // Check against detonator availability
    // Of course Arma configs don't support Boolean types
    private _isDetonatorEnabled = (["FPV", typeOf _fpvVehicle, "enableDetonator"] call TEX_fnc_cfg_getConstant) >= 1;

    if ((isNil "_isDetonatorEnabled") || !(_isDetonatorEnabled)) then {
        _fpvVehicle removeWeaponGlobal "FakeDetonator_RF";
        removeAllActions _fpvVehicle;
    };
};

_fpvVehicle setVariable ["FR_fpv_isInitialized", true];