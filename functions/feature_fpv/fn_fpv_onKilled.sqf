/*
	FR_fnc_fpv_onKilled
    Locality: Global Effect
	Author: erem2k

    Explodes FPV drone on death, emulating carried payload.

	Parameters:
	    0 - Object, RC-40 drone
    
    Returns:
        None
*/

params["_fpvDrone"];

// Pick payload type from weighted array
private _payloadTypes = flatten (["FPV", (typeOf _fpvDrone), "payloadTypes"] call TEX_fnc_cfg_getConstant);
private _payloadType = "";

if (isNil "_payloadTypes" || count _payloadTypes < 2) then {
    _payloadType = "Bo_Leaflets";
} else {
    _payloadType = selectRandomWeighted _payloadTypes;
};

// Create payload and center it on our drone
private _payload = createVehicle [_payloadType, (_fpvDrone modelToWorld [0,0,0])];

// Set operator as parent to credit the kill, there should be only one for RC40s
private _droneOperator = (UAVControl _fpvDrone) select 0;

if (!(isNull _droneOperator)) then {
    _droneOperator setShotParents [_droneOperator, _droneOperator] ;
};

// Finally, commence explosion
_payload attachto [_fpvDrone, [0,0,0]];
_payload setDamage 1;

deleteVehicle _fpvDrone;
