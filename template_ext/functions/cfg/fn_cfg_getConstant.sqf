/*
	TEX_fnc_cfg_getConstant
	Locality: Local Effect
	Author: erem2k
	
	Returns mission constant at passed path - wraps over getCfgData

	Parameters:
		0 ... N - String, config class or member key
	
	Returns:
		Any or nil when constant is not found
*/

private _configPath = [missionConfigFile, "CfgTEX", "MissionConstants", _this];

(flatten _configPath) call BIS_fnc_getCfgData;