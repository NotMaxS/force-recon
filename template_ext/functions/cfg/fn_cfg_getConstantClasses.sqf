/*
	TEX_fnc_cfg_getConstant
	Locality: Local Effect
	Author: erem2k
	
	Returns an array of subclass names for a constant - wraps over getCfgSubClasses

	Parameters:
		0 - Array, constant path
			Pass like this ["MyParam", "MySubClass"]
	
	Returns: 
		Array of String, or empty Array if requested class does not have any child classes
*/

params["_path", "_name"];

private _configPath = flatten [missionConfigFile, "CfgTEX", "MissionConstants", _path];

if (_configPath call BIS_fnc_getCfgIsClass) exitWith {
    _configPath call BIS_fnc_getCfgSubClasses;
};

[]