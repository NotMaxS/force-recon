// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point

//disable all channels beside group channel 
onPreloadFinished {

  0 enableChannel false;
  1 enableChannel false;
  2 enableChannel false;
  3 enableChannel true;
  4 enableChannel false;
  5 enableChannel false;

  // Let server know we've loaded, first player will trigger the marker promotion
  "FR_PlayerLoadedEvent" call CBA_fnc_serverEvent;

  onPreloadFinished "";
};

// Comment this line if you're looking to use a different method to set up briefings
[] execVM "scripts\briefing.sqf";

// Disable all lights on terrain
[] execVM "scripts\setup_terrain.sqf";

// Setup ACE spectator
[_player] execVM "scripts\setup_spectator.sqf"; 

//Event handler for the commando mortar counter battery script
Arty1 addEventHandler [
    "Fired", 
    {
        params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];

         if (_gunner  != player) exitWith {};

        ["SLV_PlayerArtilleryFired", [_unit, _gunner]] call CBA_fnc_serverEvent;
    }
];

