// Script for creating player tasks
// Only to be run on the server. BIS_fnc_taskCreate is global.
if (!isServer) exitWith {};

//tasks to plant the jammers
[
	[true, "zeus1", "zeus2"],
	["nidekJammer"],
	["Plant jammer in designated area near Nidek.", "Plant Jammer near Nidek", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"intel",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus1", "zeus2"],
	["raduninJammer"],
	["Plant jammer in designated area near Radunin.", "Plant Jammer near Radunin", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"intel",
	false
] call BIS_fnc_taskCreate;


//Creates Extract task and marker for said task
[
	[true, "zeus1", "zeus2"],
	["alwaysVisibleExtract"],
	["", "Extract when Ready", ""],
	[extractChopper],
	"CREATED",
	8,
	true,
	"run",
	true
] call BIS_fnc_taskCreate;