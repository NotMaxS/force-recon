// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord



if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", 
		"In case of Arma issues, there are a total of 4 mission items required for tasks:<br/>
		Jammer objective: 2 Marker Flag Black<br/> 
		HVT Obj if he dies: CDF Dogtags Inventory Item<br/> 
		Intel Obj: Kostey's Map Case<br/>
		There is one taskman in the mission, upon loading in I HIGHLY reccommend going to him and removing him from your editable 
		objects in order to avoid any funny issues due to deleting him.<br/><br/>
		This mission uses custom compositions. You can use any Chernarus Movement of Red Star and Spetznaz units and compositions, below are 
		the recommended ones:<br/><br/> 
		OPFOR->Groups->Chernarus Movement of Red Star:<br/>
		-Assault Team for full size 8 man squad<br/>
		-Air defence team for a 3 man patrol element with NVGs<br/>
		-Pilot will spawn UAV operator with a Darter and GL that can fire RC-40HE FPVs.<br/>
		These guys will be your main response unit, with little NVGs and designed to overwhelm in numbers.<br/><br/>
		OPFOR->Groups->Spetznaz:<br/> 
		-Fireteam<br/> 
		These will be your hard hitters, meant to roll up in a small group onto playerfor and fuck their shit up.<br/><br/> 
		For vehicles, use sparingly. Armour: BRDMs, BTR-80s, T-55s and T-72s.<br/> 
		Soft skin:CMoRS Hilux's, armed UAZs, Gaz Tigrs and Vodniks.<br/>
		For UAVs, use Darter, IED UAV, RC-40HE and Gundrone.<br/>
		RC-40 Smoke White grenades
		are scripted to function as anti-tank drones. Do not use the End key or Detonate scroll wheel option when using them as 
		this can cause unintended behaviour, only impact detonation. You will have to manually control all FPV drones, do not
		abuse them against players as they will instantly kill them.<br/>
		Use Gundrones ONLY WITH REDUCED ACCURACY.<br/> 
		Use Darters to spot playerfor and send patrols onto their last known location.<br/> 
		All indirect fire assets in this op are listed as LAMBs artillery. Counter-battery against the mortar will be handled
		by the mission automatically. No need to add  to it.<br/>
		Additionally, you can spawn enemy planes and heavier indirect fire support to either hit areas where players used to be or 
		hit nearly friendly lines to create battle ambience and remind the players what's out there. Use SU25 and SU34."]];
	};

player createDiaryRecord ["Diary", ["Assets", "Asset List:<br/>
	- 1x Offroad Comms Command Vehicle<br/>
	- 1x Pickup Comms Drone Operator Vehicles<br/>
	- 1x Pickup Regular Drone Operator Vehicles (Mortar Rearm Vehicle)<br/>
	- 1x Sniper Team Quadbike<br/>
	- 2x Unarmed Prowler, Gorilla and Baboon Squad<br/>
	- x1 Offroad UP, Orangutan Squad<br/>
	- x1 60mm Commando Mortar (Extra ammo in Drone Operator Pickup)<br/> 
	- x14 Darter UAVs<br/> 
	- x5 Drop UAVs<br/> 
	- x4 Pelican UAVs<br/> 
	- x25 RC-40HE FPVs<br/> 
	- x15 RC-40 Anti-Tank FPVs (Displayed in-game as RC-40 Smoke White.) 
"]];


player createDiaryRecord ["Diary", ["Intel: Enemies",
"You are expected to face:<br/> 
1) Elements of the 151st mech. brigade. Armed with AK74s, AK12s, PKMs, SVDs and RPG-7s. Low to Moderate amount of NVGs. 
Will use crew-served weapons to include Kords HMGs, Kornet ATGMs, 60mm and 82mm mortars. 
Will have counter-battery capabilities. Equipped with technicals/soft skin vehicles, BRDMs, BTR-80s, T-55s and T-72s.<br/> 
2) Elements of the 151st mech. brigade's UAV company. Equipped with Tayran AR-2 Surveillance Drones, Grenade Drop Drones, Roshanak 
AP-5 Gun Drones and FPV drones.<br/> 
3) SOF elements. Equipement and weaponry unknown. Believed to be operating in 4 man teams with silenced weaponry and NVGs.
No other information is available.<br/> 
Elements of enemy tactical aviation are known to be operating in your AO. Unlikely to engage the task force due to small squad size but 
cannot be discounted."
]];

player createDiaryRecord ["Diary", ["Intel: Pictures",
"Picture of Radar:<br/><img image='media\missionRadarPicture.jpg'/><br/>
Picture of HVT:<br/><img image='media\missionHVTPicture.jpg'/><br/>  
"
]];

player createDiaryRecord ["Diary", ["Victory Conditions",
"Out of the follow up tasks:<br/>
4 Completed: Victory<br/>
3 Completed: Partial Victory<br/>
2 Completed: Minor Loss<br/>
1 or 0 Completed: Loss<br/><br/>
The Jammers are a pre-requisite to unlock the tasks, they do not count towards any victory conditions."
]];


player createDiaryRecord ["Diary", ["Intel: Tasks",
"Initial Task:<br/> 
Plant the two jammers, one in each <marker name = 'FR_jammer_objArea_1'>designated</marker> <marker name = 'FR_jammer_objArea_2'>circle</marker>.
Once both jammers are placed down, you will be able to complete the rest of the assigned tasks. The jammers MUST be placed BEFORE 
any task below can be attempted.<br/><br/>
Follow Up Tasks:<br/>
<marker name = 'marker_34'> -Destroy SPAA and Radar:</marker> The SPAA will be a 2S6 Tunguska. It is believed to be operating from
an underground bunker and only deploying out when the radar spots a target.<br/> 
<marker name = 'marker_33'>-Capture HVT:</marker> An enemy company commander is operating in the town of Lipina. He will
be in an intact building somewhere on the North/North-West side of the town. Bring him to the
<marker name = 'marker_39'> HVT Collection Point.</marker> If he dies, his uniform will have an important Intel Item in it. Loot it and 
bring it to the <marker name = 'marker_39'> HVT Collection Point.</marker><br/> 
<marker name = 'marker_32'>Damaged Tank:</marker> During a friendly armoured battlegroup push, a M1A1SA TUSK MBT was damaged but not lost. 
An enemy <marker name = 'marker_21'>Logistics Hub</marker> is operating in the town of Nidek. Acquire a Fuel Truck and a Repair
Truck from there then head to the tank. Then drive it to the <marker name = 'marker_39'> HVT Collection Point.</marker>.<br/>
<marker name = 'marker_35'>-Intel:</marker> There is a forward enemy command post operating near the town of Huta. It
will be to the East/North East of the town. Raid the command post and locate the Intel Item, a Map Case. Bring this to the <marker name = 'marker_64'> Intel Point</marker>.<br/>
"
]];


player createDiaryRecord ["Diary", ["Mission",
"In order to carry out precise strikes against enemy targets behind enemy lines, their lines of communication to adjacent units must
be cut off. This will prevent the enemy from sounding the alarm and bringing in heavier assets. Local enemy QRF will still be present in 
the AO. Once comms are disrupted, the Task Force will clear the objectives while simultaneously moving back towards friendly lines.<br/><br/> 
Dispersion and stealth, breaking contact and avoiding decisive engagements are your orders. Do not go outside the AO borders
or past the Limit of Exploitation marked on the map.<br/><br/>
Bunched up squads, squads in the open or squads in contact will quickly attract enemy reinforcements. Enemy UAV presence in the AO is at an all time high, 
enemy drones will either engage you and/or attract enemy forces and indirect fire to your location.<br/><br/>
Commandeering enemy transport vehicles and AT weapons is greenlit if necessary, squadleader's discretion.
Looting of any other sort will be up to your platoon leader as resupply will be available through Pelican Drones.<br/><br/> 
Friendly drone and mortar assets are encouraged to take out enemy drone operators and mortar assets if spotted. Expect counter-battery 
fire after using your mortar."
]];

player createDiaryRecord ["Diary", ["Situation",
"Recon elements of the 53d Infantry Brigade have identified and roughly geolocated key enemy assets,
equipement and personnel in the AO. While the enemy BTG's Main Effort is diverted from this area, High Command wishes
to carry out a series of decisive strike in order to make rapid tactical gains in the region.
Task Force 'TMTM' has been assigned to this mission and is expected to begin carrying out operations NLT 2100 hours."
]];

player createDiaryRecord ["Diary", ["Game Mechanics",
"1. Markers:<br/>
Once the mission starts, 
all players will only have access to the Group Channel for drawing and marker placing. The markers and lines drawn on
the map in Global during the planning phase will remain and be visible. During the in-game phase
players will only be able to share information via map screen within their assigned groups. 
If groups become co-located or merged due to losses/player count, you can join the
other group as long as you are in proximity to them. Joining another group while not being in proximity with them to share information
is considered metagaming.<br/><br/> 
2. Jammer placement:<br/>
In order to complete the marker objectives, you will need a jammer item (Marker Flag Black). Two of these will be available
in a crate at mission start, one for each area. Once you are in the zone, you will have a self-hold action appear if:<br/>  
- Player has the item in their inventory<br/>
- Player is not in a vehicle<br/>
- Player is standing still<br/><br/>
3. Intel Items:<br/> 
They will be inventory items that you either take out of the HVT's Uniform or pickup at the Intel Objective, one per each objective. 
Once you have the item, simply go to the zone indicated by the task marker. The task will complete when you are in the zone and the
item will be removed from your inventory.<br/><br/>
4. Extraction:<br/> 
You can extract at any point in the mission. Walk up to the helicopter at your start location, get into a middle passenger seat and
complete the hold action.<br/><br/>
5. View Distance:<br/> For Dynasim and performance purposes, View Distance has been set at 1500m via Ace View Distance."
]];
