// setup_terrain.sqf
// Disables all lights on the map, must be ran locally
// Arguments - None
// Returns - None

// Disables pre-placed lights
{
    _x switchLight "OFF";
} forEach (1 allObjects 0);
