// setup_spectator.sqf
// Sets up spectator mode
// Arguments
// 0 - Object, player - player entity to set up spectator settings for
// Returns - None

params["_player"];

//let players have almost all vision modes, have all camera modes and see units from all sides
[[-2, -1, 0, 1], [ 2, 3, 4, 5, 6, 7]] call ace_spectator_fnc_updateVisionModes;
[[0,1,2], []] call ace_spectator_fnc_updateCameraModes;
[[west, east, independent, civilian], []] call ace_spectator_fnc_updateSides; 