// Script for creating player tasks mid-mission, is triggered by in-game triggers 
//ExecVM'd in trigger init in editor 
// Only to be run on the server. BIS_fnc_taskCreate is global.
if (!isServer) exitWith {};

//collection of tasks for inside Radunin, parented under one task
[
	[true, "zeus1", "zeus2"],
	["raduninSPAA", "raduninAA"],
	["", "Destroy the SPAA", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"destroy",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus1", "zeus2"],
	["raduninAA"],
	["", "Destroy the SPAA and AA Radar in Radunin", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"destroy",
	false
] call BIS_fnc_taskCreate;

[
	[true, "zeus1", "zeus2"],
	["raduninAARadar", "raduninAA"],
	["", "Destroy the Radar", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"destroy",
	false
] call BIS_fnc_taskCreate;

//Optional task to recover the destroyed tank, creates marker on where to bring the tank once it's repaired
[
	[true, "zeus1", "zeus2"],
	["recoverTank"],
	["", "Bring the Damaged Tank Here", ""],
	[recoverTank_Pos],
	"CREATED",
	10,
	true,
	"land",
	true
] call BIS_fnc_taskCreate;

//Creates marker for where to bring HVT once he has been kidnapped from Lipina
[
	[true, "zeus1", "zeus2"],
	["grabHVT"],
	["", "Bring the HVT Here", ""],
	[grabHVT_Pos],
	"CREATED",
	10,
	true,
	"kill",
	true
] call BIS_fnc_taskCreate;

//Creates marker for where to bring intel from Huta once it has been acquired
[
	[true, "zeus1", "zeus2"],
	["grabIntel"],
	["", "Bring the Intel from Huta Here", ""],
	[grabIntel_Pos],
	"CREATED",
	10,
	true,
	"documents",
	true
] call BIS_fnc_taskCreate;

//destroy the command vehicle in muratyn
/*
[
	[true, "zeus1", "zeus2"],
	["muratynCV"],
	["", "Destroy the EW Vehicle in Muratyn", ""],
	[objNull],
	"CREATED",
	10,
	true,
	"destroy",
	false
] call BIS_fnc_taskCreate;
/*  
