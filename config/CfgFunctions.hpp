// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

// If you need to add additional functions, create a new section below using your tag of choice (Ex: SXP = Superxpdude)
// See the functions library wiki page for additional details

class SXP // Function TAG, used for the first part of the function name
{
	class vehicle // Function category, defines the folder that the file is located in
	{
		class setupVehicle {}; // Function class. Defines the file 
	};

}; 

class FR
{
	class feature_fpv
	{
		class fpv_onKilled {};
		class fpv_onVehicleInit {};
	};
	
	class feature_itemSet
	{
		class itemSet_diag {};
		class itemSet_findSetCarrier {};
		class itemSet_hasSet {};
		class itemSet_onInit { postInit = 1; };
		class itemSet_onPlayerLoadoutChanged {};
		class itemSet_onPlayerRespawn {};
		class itemSet_removeSet {};
		class itemSet_submitSetUpdate {};
	};

	class feature_jammer
	{
		class jammer_canCarry {};
		class jammer_canDeploy {};
		class jammer_isJammerInArea {};
		class jammer_onActionDeploy {};
		class jammer_onDeployStarted {};
		class jammer_onDeployCompleted {};
		class jammer_onInit { postInit = 1; };
		class jammer_onPlayerRespawn {};
	};

	// class feature_uavDetector
	// {
	// 	class uavDetector_onPlayerRespawn {};
	// 	class uavDetector_sfxLoop {};
	// };

	class feature_spectator 
	{
		class spectator_setupSpectator {}; 
	};

	class feature_markers
	{
		class markers_bulkCreateLayer {};
		class markers_bulkMoveLayer {};
	};

	class mission 
	{
		class mission_onTaskCompleted{}; 
	}; 

};

class TG
{
	class deform
	{
		class terrainDeform {};
	};
};

class SLV 
{
	class mortar 
	{
		class counterBattery {}; 
	};

	class mission 
	{
		class endMissionLogic{}; 
	}; 
	
};
