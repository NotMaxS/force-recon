class PlayerUniform
{
    class CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_1
    {
        class Top
        {
            class Forest
            {
                weight = 1.0;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoJacket_forest_co.paa",
                    "media\camos\tmtm_cdf_vkpoJacketHands_forest_co.paa"
                };
            };

            class Mountain
            {
                weight = 0.7;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoJacket_mountain_co.paa",
                    "media\camos\tmtm_cdf_vkpoJacketHands_mountain_co.paa"
                };
            };

            class Oxblood
            {
                weight = 1.0;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoJacket_oxblood_co.paa",
                    "media\camos\tmtm_cdf_vkpoJacketHands_oxblood_co.paa"
                };
            };

            class MTP
            {
                weight = 0.5;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoJacket_mtp_co.paa",
                    "media\camos\tmtm_cdf_vkpoJacketHands_mtp_co.paa"
                };
            };

            class Black
            {
                weight = 0.2;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoJacket_black_co.paa",
                    "media\camos\tmtm_cdf_vkpoJacketHands_black_co.paa"
                };
            };
        };
        class Botton
        {
            class Forest
            {
                weight = 1.0;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoPants_forest_co.paa"
                };
            };

            class Mountain
            {
                weight = 0.7;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoPants_mountain_co.paa"
                };
            };

            class Oxblood
            {
                weight = 1.0;

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoPants_oxblood_co.paa"
                };
            };

            class MTP
            {
                weight = 0.5;
                excludedTops[] = { "MTP" };

                hiddenSelections[] = {
                    "media\camos\tmtm_cdf_vkpoPants_mtp_co.paa"
                };
            };
        };
    };

    class CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_2 : CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_1 {};

    class CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_3 : CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_1 {};
    
    class CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_4 : CUP_U_O_RUS_Soldier_VKPO_Jacket_Winter_1 {};   
};