class FPV
{
    /*
    * Global fuel consumption rate override for FPVs.
    */
    fuelConsumptionCoef = 0.5;

    /*
    * Configuration of available payload types, per UAV class.
    */
    class B_UAV_RC40_SmokeWhite_RF
    {
        /*
        * Array of pairs, where first member is the name of CfgAmmo class, and second is it's relative weight to the rest of set.
        *   On Killed handler will randomly pick one of these; the higher the weight, the higher is the probability.
        */
        payloadTypes[] = { {"CUP_R_PG7V_AT", 0.9}, {"CUP_R_PG7VR_AT", 0.1} };

        /*
        * Whether to enable RF detonator on unit.
        *   NOTE: With rocket/missile-like payloads this will cause them to be *launched* where UAV is pointing at.
        */
        enableDetonator = 0;
    };

    class O_UAV_RC40_SmokeWhite_RF : B_UAV_RC40_SmokeWhite_RF {};
    class I_UAV_RC40_SmokeWhite_RF : B_UAV_RC40_SmokeWhite_RF {};
    class C_UAV_RC40_SmokeWhite_RF : B_UAV_RC40_SmokeWhite_RF {};
};