class ItemSet
{
    /*
    * Network synchronization delay for per-unit item sets, in seconds.
    */
    syncDelay = 4.0;

    /*
    * Item sets definitions; use their names when refering to then in hasSet, etc.
    */
    class JammerObjective
    {
        /*
        * Classnames of items required to complete this set.
        */
        items[] = {"ace_marker_flags_black"};
    };

    class HVTObjective
    {
        /*
        * Classnames of items required to complete this set.
        */
        items[] = {"CUP_item_CDF_dogtags"};
    };
    
    class BunkerIntelObjective
    {        
        /*
        * Classnames of items required to complete this set.
        */
        items[] = {"CUP_item_Kostey_map_case"};
    };
};