class Jammer
{
    /*
    * Classname of jammer object in world.
    */
    worldObject = "SatelliteAntenna_01_Small_Black_F";

    /*
    * Name format of jammer objective areas.
    */
    objMarkerTemplate = "FR_jammer_objArea_";

    /*
    * Configuration related to jammer actions.
    */
    class Action
    {
        /*
        * Mission path to jammer deployment SFX file.
        */
        sfxPath = "media\sfx\jammer_deploy.ogg";

        /*
        * Duration of jammer deployment SFX, will also influence hold action duration.
        */
        sfxDuration = 5.0;
    };

    class Ambient
    {
        /*
        * CfgVehicles class of jammer ambient SFX.
        */
        sfxType = "FR_Jammer_AmbientLoopSound";
    };
};