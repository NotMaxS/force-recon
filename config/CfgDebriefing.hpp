// Mission endings
// Handles the mission ending screen
// https://community.bistudio.com/wiki/Debriefing


class youDidNotWin  
{
	title = "Major Loss"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "You failed to accomplish anything significant"; // Subtitle below the title when the closing shot is triggered
	description = "Do better next time."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class youHadMinorLoss  
{
	title = "Partial Loss"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "You accomplished some objectives."; // Subtitle below the title when the closing shot is triggered
	description = "Do better next time."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class youPartiallyWon  
{
	title = "Partial Victory"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "You accomplished most objectives"; // Subtitle below the title when the closing shot is triggered
	description = "Do better next time."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};

class youTotallyWon  
{
	title = "Victory"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "You accomplished all the objectives"; // Subtitle below the title when the closing shot is triggered
	description = "Do better next time."; // Description visible on the debriefing screen after the closing shot
	//pictureBackground = ""; // Image file used as a background on the debriefing screen
	//picture = ""; // Icon used for the closing shot
	//pictureColor[] = {1,1,1,1}; // Colour of the icon during the closing shot. Leave as default most of the time
};
