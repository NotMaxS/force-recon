// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.


class itemCargos
{
	class dronePickup 
	{
		items[] = {
			{"CUP_HandGrenade_M67", 4},
			{"SmokeShell", 8},
			{"ToolKit", 2},
			{"B_AssaultPack_khk", 2}, 
			{"ACE_UAVBattery", 50}
		};

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 15},
			{"ACE_epinephrine", 10},
			{"ACE_morphine", 10}, 
			{"ACE_bloodIV_500",4}, 
			{"ACE_tourniquet",8},
			{"ACE_splint",8}
		};
	}; 

	class mortarPickup 
	{
		items[] =
		{	
			{"CUP_30Rnd_556x45_PMAG_BLACK_PULL", 10}, 
			{"CUP_HandGrenade_M67",4},
			{"SmokeShell", 8},
			{"ToolKit",2},
			{"B_AssaultPack_khk", 2}, 
			{"ACE_Chemlight_HiBlue", 8},
			{"ACE_Chemlight_HiGreen", 8},
			{"ACE_Chemlight_HiRed", 8}, 
			{"ACE_Chemlight_HiWhite", 8},
			{"B_IR_Grenade", 8}
			
		}; 
		
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 15},
			{"ACE_epinephrine", 10},
			{"ACE_morphine", 10}, 
			{"ACE_bloodIV_500",4}, 
			{"ACE_tourniquet",8},
			{"ACE_splint",8}
		};
	}; 

	class sniperATV 
	{
		items[] = {	
			{"CUP_30Rnd_556x45_PMAG_BLACK_PULL", 6}, 
			{"CUP_HandGrenade_M67",4},
			{"SmokeShell", 4},
			{"ToolKit",1},
			{"B_AssaultPack_khk", 1}, 
			{"ACE_Chemlight_HiBlue", 4},
			{"ACE_Chemlight_HiGreen", 4},
			{"ACE_Chemlight_HiRed", 4}, 
			{"ACE_Chemlight_HiWhite", 4},
			{"CUP_5Rnd_762x67_M2010_M", 10}
			
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 8},
			{"ACE_epinephrine", 2},
			{"ACE_morphine", 2},  
			{"ACE_tourniquet",4},
			{"ACE_splint",2}
		};
	}; 

	
	class brokenTank  
	{
		items[] = {	
			{"SmokeShell", 8},
			{"ToolKit",2}			
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 8},
			{"ACE_epinephrine", 2},
			{"ACE_morphine", 2},  
			{"ACE_tourniquet",4},
			{"ACE_splint",1}
		};
	};

	 class squadVehicle 
	{
		items[] = {	
			{"CUP_30Rnd_556x45_PMAG_BLACK_PULL", 8}, 
			{"CUP_launch_M136",3}, 
			{"ACE_launch_NLAW_ready_F", 1}, 
			{"ToolKit",1}, 
			{"CUP_HandGrenade_M67",6},
			{"SmokeShell", 4},
			{"ACE_Chemlight_HiBlue", 4},
			{"ACE_Chemlight_HiGreen", 4},
			{"ACE_Chemlight_HiRed", 4}, 
			{"ACE_Chemlight_HiWhite", 4},
			{"B_IR_Grenade", 4}, 
			{"ACE_CableTie", 4}, 
			{"ACE_CTS9", 2},
			{"CUP_100Rnd_556x45_BetaCMag_ar15", 4}, 
			{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS", 8}, 
			{"1Rnd_HE_Grenade_shell", 6}, 
			{"1Rnd_Smoke_Grenade_shell", 6}, 
			{"Laserbatteries", 4},
			{"ACE_Clacker",1},
			{"ClaymoreDirectionalMine_Remote_Mag", 4},
			{"CUP_PipeBomb_M", 1}, 
			{"B_AssaultPack_rgr", 1}
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 12},
			{"ACE_epinephrine", 5},
			{"ACE_morphine", 5},
			{"ACE_tourniquet",8},
			{"ACE_splint",8}
		};
	}; 
};