// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	//standardized blufor basic medical, randomized facial gear for slav loadouts 
	#define BLU_MEDICAL_STANDARD {"ACE_quikclot",12},{"ACE_epinephrine",2},{"ACE_morphine",4},{"ACE_splint",1},{"ACE_tourniquet",4}
	#define BLU_FACEWEAR "G_Balaclava_TI_blk_F", "G_Bandanna_oli", "G_Bandanna_beast", "G_Bandanna_tan", "G_Balaclava_snd_lxWS", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_1"
	
	//standardized link items for leadership and non-leadership roles
	#define LINKED_ITEMS_LDR "ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch","ACE_NVG_Wide_Black_WP"; 
	#define LINKED_ITEMS_SUB "ItemMap","ItemGPS","ItemRadio","ItemCompass","TFAR_microdagr","ACE_NVG_Wide_Black_WP";
	#define LINKED_ITEMS_CDR "ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch","ACE_NVG_Wide_Black_WP";

	//standardized items for quick changes to unit's inventory that i needed to do lol
	#define UNIFORM_STANDARD {"ItemcTabHCam",1}, {"ACE_CableTie",2},{"ACE_CTS9",2,1}, {"ACE_IR_Strobe_Item",1};
	#define UNIFORM_STANDARD_ALT {"ItemcTabHCam",1}, {"ACE_CableTie",2}, , {"ACE_IR_Strobe_Item",1}; 
	#define UNIFORM_STANDARD_CLS {"hlc_15Rnd_9x19_B_P226",2,15}, {"ItemcTabHCam",1}, {"ACE_CableTie",2} , {"ACE_IR_Strobe_Item",1};
	#define EXP_SPEC_BACKPACK {"CUP_HandGrenade_M67",2},{"SmokeShell",2 },{"ACE_Chemlight_HiBlue",2},{"ACE_Chemlight_HiGreen",2},{"ACE_Chemlight_HiRed",2},{"ACE_Chemlight_HiWhite",2},{"B_IR_Grenade",2}, {"ACE_DefusalKit",1},{"ACE_Clacker",2},{"MineDetector",1},{"ClaymoreDirectionalMine_Remote_Mag",4,1},{"CUP_PipeBomb_M",2};

	// trenchgun loadouts
	#define TG_CAMO_NETTING "tmtm_f_camoNetting_arid_1", "tmtm_f_camoNetting_arid_2", "tmtm_f_camoNetting_arid_3", "tmtm_f_camoNetting_arid_4", "tmtm_f_camoNetting_arid_5", "tmtm_f_camoNetting_semiArid_1"
	#define TG_HEADGEAR_A "tmtm_h_fastHeadset_olive", "H_HelmetB_camo", "tmtm_h_helmetEnhanced_olive", "H_HelmetSpecB", "H_HelmetSpecB_paint1", "H_HelmetSpecB_snakeskin", "H_HelmetSpecB_wdl", "tmtm_h_helmetLight_olive", "H_HelmetB_light", "H_HelmetB_light_grass", "H_HelmetB_light_snakeskin", "H_HelmetB_light_wdl", "CUP_H_CZ_Helmet07", "CUP_H_CZ_Helmet10", "CUP_H_BAF_MTP_Mk7_PRR_SCRIM_B", "CUP_H_BAF_MTP_Mk7_PRR_SCRIM_A", "CUP_H_BAF_MTP_Mk6_EMPTY_PRR", "CUP_H_BAF_MTP_Mk7_PRR", "CUP_H_OpsCore_Grey", "CUP_H_OpsCore_Covered_MTP", "CUP_H_OpsCore_Covered_MCAM", "CUP_H_OpsCore_Spray", "CUP_H_OpsCore_Grey_SF", "CUP_H_OpsCore_Covered_MTP_SF", "CUP_H_OpsCore_Covered_MCAM_SF", "CUP_H_OpsCore_Spray_SF", "CUP_H_USArmy_HelmetACH_ESS_Headset_OCP", "CUP_H_USArmy_HelmetACH_Headset_OCP", "tmtm_h_ECHHeadset_cdfForest", "tmtm_h_ECHGogglesHeadset_cdfForest", "tmtm_h_ECHHeadset_cdfMountain", "tmtm_h_ECHGogglesHeadset_cdfMountain", "tmtm_h_ECHHeadset_cdfOxblood", "tmtm_h_ECHGogglesHeadset_cdfOxblood", "tmtm_h_ECHHeadset_mtp", "tmtm_h_ECHGogglesHeadset_mtp"
	#define TG_HEADGEAR_B "CUP_H_USArmy_HelmetACH_OCP", "CUP_H_USArmy_HelmetACH_ESS_OCP", "CUP_H_USArmy_HelmetACH_OEFCP", "CUP_H_USArmy_HelmetACH_ESS_OEFCP", "H_HelmetB", "H_HelmetB_grass", "tmtm_h_helmet_olive", "H_HelmetB_snakeskin", "H_HelmetB_plain_wdl", "H_HelmetB_plain_sb_mtp_RF", "H_HelmetB_plain_sb_wdl_RF", "CUP_H_USArmy_ECH_MARPAT", "CUP_H_USArmy_ECH_ESS_MARPAT", "CUP_H_LWHv2_OD", "CUP_H_LWHv2_MARPAT", "CUP_H_LWHv2_MARPAT_NVG_gog2_cov2", "CUP_H_BAF_DPM_Mk6_EMPTY", "CUP_H_BAF_MTP_Mk6_EMPTY", "CUP_H_BAF_MTP_Mk7", "CUP_H_OpsCore_Grey_NoHS", "CUP_H_OpsCore_Covered_MTP_NoHS", "CUP_H_OpsCore_Covered_MCAM_NoHS", "CUP_H_OpsCore_Spray_NoHS"
	#define TG_FACEWEAR_A "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_1", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_2", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_3", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_4", "CUP_G_RUS_Ratnik_Balaclava_Olive_1", "CUP_G_RUS_Ratnik_Balaclava_Olive_2", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_1", "G_Bandanna_khk", "G_Bandanna_oli", "G_Bandanna_Skull1", "G_Bandanna_Skull2", "G_Bandanna_beast", "CUP_Beard_Black"
	#define TG_FACEWEAR_B "tmtm_f_balaclava_ratnik_headphones", "tmtm_f_balaclava_ratnik_headphones_v2", "tmtm_f_GSSh01_headphones"
	#define TG_UNIFORM_1 "tmtm_u_vkpo_jacket_winter_blackForest_1", "tmtm_u_vkpo_jacket_winter_blackMountain_1", "tmtm_u_vkpo_jacket_winter_blackOxblood_1", "tmtm_u_vkpo_jacket_winter_blackMTP_1", "tmtm_u_vkpo_jacket_winter_Forest_1", "tmtm_u_vkpo_jacket_winter_ForestMountain_1", "tmtm_u_vkpo_jacket_winter_ForestMTP_1", "tmtm_u_vkpo_jacket_winter_ForestOxblood_1", "tmtm_u_vkpo_jacket_winter_Mountain_1", "tmtm_u_vkpo_jacket_winter_MountainForest_1", "tmtm_u_vkpo_jacket_winter_MountainMTP_1", "tmtm_u_vkpo_jacket_winter_MountainOxblood_1", "tmtm_u_vkpo_jacket_winter_Oxblood_1", "tmtm_u_vkpo_jacket_winter_OxbloodForest_1", "tmtm_u_vkpo_jacket_winter_OxbloodMountain_1", "tmtm_u_vkpo_jacket_winter_OxbloodMTP_1", "tmtm_u_vkpo_jacket_winter_MTPForest_1", "tmtm_u_vkpo_jacket_winter_MTPMountain_1", "tmtm_u_vkpo_jacket_winter_MTPOxblood_1" 
	#define TG_UNIFORM_2 "tmtm_u_vkpo_jacket_winter_blackForest_2", "tmtm_u_vkpo_jacket_winter_blackMountain_2", "tmtm_u_vkpo_jacket_winter_blackOxblood_2", "tmtm_u_vkpo_jacket_winter_blackMTP_2", "tmtm_u_vkpo_jacket_winter_Forest_2", "tmtm_u_vkpo_jacket_winter_ForestMountain_2", "tmtm_u_vkpo_jacket_winter_ForestMTP_2", "tmtm_u_vkpo_jacket_winter_ForestOxblood_2", "tmtm_u_vkpo_jacket_winter_Mountain_2", "tmtm_u_vkpo_jacket_winter_MountainForest_2", "tmtm_u_vkpo_jacket_winter_MountainMTP_2", "tmtm_u_vkpo_jacket_winter_MountainOxblood_2", "tmtm_u_vkpo_jacket_winter_Oxblood_2", "tmtm_u_vkpo_jacket_winter_OxbloodForest_2", "tmtm_u_vkpo_jacket_winter_OxbloodMountain_2", "tmtm_u_vkpo_jacket_winter_OxbloodMTP_2", "tmtm_u_vkpo_jacket_winter_MTPForest_2", "tmtm_u_vkpo_jacket_winter_MTPMountain_2", "tmtm_u_vkpo_jacket_winter_MTPOxblood_2" 
	#define TG_UNIFORM_3 "tmtm_u_vkpo_jacket_winter_blackForest_3", "tmtm_u_vkpo_jacket_winter_blackMountain_3", "tmtm_u_vkpo_jacket_winter_blackOxblood_3", "tmtm_u_vkpo_jacket_winter_blackMTP_3", "tmtm_u_vkpo_jacket_winter_Forest_3", "tmtm_u_vkpo_jacket_winter_ForestMountain_3", "tmtm_u_vkpo_jacket_winter_ForestMTP_3", "tmtm_u_vkpo_jacket_winter_ForestOxblood_3", "tmtm_u_vkpo_jacket_winter_Mountain_3", "tmtm_u_vkpo_jacket_winter_MountainForest_3", "tmtm_u_vkpo_jacket_winter_MountainMTP_3", "tmtm_u_vkpo_jacket_winter_MountainOxblood_3", "tmtm_u_vkpo_jacket_winter_Oxblood_3", "tmtm_u_vkpo_jacket_winter_OxbloodForest_3", "tmtm_u_vkpo_jacket_winter_OxbloodMountain_3", "tmtm_u_vkpo_jacket_winter_OxbloodMTP_3", "tmtm_u_vkpo_jacket_winter_MTPForest_3", "tmtm_u_vkpo_jacket_winter_MTPMountain_3", "tmtm_u_vkpo_jacket_winter_MTPOxblood_3" 
	#define TG_VEST "CUP_V_CPC_communications_rngr", "CUP_V_CPC_medical_rngr", "CUP_V_CPC_tl_rngr", "CUP_V_CPC_weapons_rngr", "CUP_V_JPC_communications_mc", "CUP_V_JPC_medical_mc", "CUP_V_JPC_tl_mc", "CUP_V_JPC_weapons_mc", "CUP_V_JPC_communications_rngr", "CUP_V_JPC_medical_rngr", "CUP_V_JPC_tl_rngr", "CUP_V_JPC_weapons_rngr", "CUP_V_B_Delta_1", "CUP_V_B_Delta_2", "CUP_V_B_BAF_DPM_Osprey_Mk3_Engineer", "CUP_V_B_BAF_DPM_Osprey_Mk3_Medic", "CUP_V_B_BAF_DPM_Osprey_Mk3_Officer", "CUP_V_B_BAF_DPM_Osprey_Mk3_Rifleman", "CUP_V_B_BAF_DPM_Osprey_Mk3_Scout", "tmtm_v_ospreyMk3_engineer_mtp", "tmtm_v_ospreyMk3_medic_mtp", "tmtm_v_ospreyMk3_officer_mtp", "tmtm_v_ospreyMk3_rifleman_mtp", "tmtm_v_ospreyMk3_scout_mtp", "tmtm_v_ospreyMk4_engineer_noflag", "tmtm_v_ospreyMk4_medic_noflag", "tmtm_v_ospreyMk4_rifleman_noflag"
	#define TG_VEST_AR "CUP_V_B_BAF_DPM_Osprey_Mk3_AutomaticRifleman", "tmtm_v_ospreyMk3_automaticRifleman_mtp", "tmtm_v_ospreyMk4_automaticRifleman_noflag", "V_Simc_vest_rba_mk1_alice_249", "V_Simc_vest_pasgt_grun_alice_249"
	#define TG_VEST_GL "CUP_V_B_BAF_DPM_Osprey_Mk3_Grenadier", "tmtm_v_ospreyMk3_grenadier_mtp", "tmtm_v_ospreyMk4_grenadier_noflag", "tmtm_v_ospreyMk4_officer_noflag"
	#define TG_BACKPACK_SMALL "B_AssaultPack_blk", "B_AssaultPack_mcamo", "B_TacticalPack_blk", "B_CivilianBackpack_01_Everyday_Black_F", "CUP_B_USMC_AssaultPack", "B_FieldPack_green_F", "tmtm_b_ospreyBelt"
	#define TG_BACKPACK_LARGE "CUP_B_USPack_Coyote", "tmtm_b_motherlode_MTPnoflag", "tmtm_b_predator_MTPnoflag", "CUP_B_USMC_MOLLE", "CUP_B_USMC_MOLLE_WDL", "B_Carryall_green_F", "B_Carryall_mcamo", "B_Carryall_wdl_F", "B_Kitbag_mcamo"
	#define TG_BACKPACK_XLARGE "tmtm_b_motherlode_MTPnoflag", "CUP_B_USMC_MOLLE", "CUP_B_USMC_MOLLE_WDL", "B_Carryall_green_F", "B_Carryall_mcamo", "B_Carryall_wdl_F"
	#define TG_BACKPACK_RADIO "tmtm_b_motherlodeRadio_MTPnoflag", "tmtm_b_predatorRadio_MTPnoflag"
	
	// Leadership Primary Weapons (magnified optics)
	#define TG_SL_PRIMARY_HK416 {"CUP_arifle_HK416_Wood","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_HK416CQB {"CUP_arifle_HK416_CQB_Wood","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M16A4GRIP {"CUP_arifle_M16A4_Grip","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M16A4 {"CUP_arifle_M16A4_Base","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M27GRIP {"CUP_arifle_HK_M27","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M27 {"CUP_arifle_HK_M27_VFG","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M4A1GRIP {"CUP_arifle_M4A3_camo","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_HensoldtZO_RDS_od",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M4A1 {"CUP_arifle_M4A1_camo","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_HensoldtZO_RDS_od",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M4A1SOPMOD {"CUP_arifle_M4A1_SOMMOD_green","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_HensoldtZO_RDS_od",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_MK12 {"CUP_srifle_Mk12SPR","CUP_muzzle_snds_Mk12","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_MK16CQC {"CUP_arifle_Mk16_CQC_AFG_woodland","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_MK16 {"CUP_arifle_Mk16_STD_AFG_woodland","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_MK18 {"CUP_arifle_mk18_black","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_M4SBR {"CUP_arifle_SBR_od","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_blk_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_SPAR {"arifle_SPAR_01_khk_F","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_ACR {"CUP_arifle_ACR_wdl_556","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_ACRC {"CUP_arifle_ACRC_wdl_556","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_BREN {"CUP_CZ_BREN2_556_8_Grn","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Arco_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_TRG20 {"arifle_TRG20_F","muzzle_snds_M","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_SL_PRIMARY_TRG21 {"arifle_TRG21_F","muzzle_snds_M","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}

	// AR Primary Weapons
	#define TG_AR_PRIMARY_M27GRIP {"CUP_arifle_HK_M27_VFG","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","CUP_optic_CompM2_Black",{"CUP_100Rnd_556x45_BetaCMag_ar15",100},{},"CUP_bipod_Harris_1A2_L_BLK"}
	#define TG_AR_PRIMARY_M27 {"CUP_arifle_HK_M27","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","CUP_optic_CompM4",{"CUP_100Rnd_556x45_BetaCMag_ar15",100},{},"CUP_bipod_Harris_1A2_L_BLK"}
	#define TG_AR_PRIMARY_SPARS {"arifle_SPAR_02_khk_F","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","CUP_optic_CompM4",{"CUP_100Rnd_556x45_BetaCMag_ar15",100},{},"CUP_bipod_Harris_1A2_L_BLK"}
	#define TG_AR_PRIMARY_MK16SV {"CUP_arifle_Mk16_SV_woodland","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","CUP_optic_CompM4",{"CUP_100Rnd_556x45_BetaCMag_ar15",100},{},"CUP_bipod_Harris_1A2_L_BLK"}
	

	// Standard Primary Weapons (non-magnified optics)
	#define TG_PRIMARY_HK416 {"CUP_arifle_HK416_Wood","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_1P87_RIS_woodland",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_HK416CQB {"CUP_arifle_HK416_CQB_Wood","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_1P87_RIS_woodland",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M16A4GRIP {"CUP_arifle_M16A4_Grip","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M16A4 {"CUP_arifle_M16A4_Base","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M27GRIP {"CUP_arifle_HK_M27","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M27 {"CUP_arifle_HK_M27_VFG","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M4A1GRIP {"CUP_arifle_M4A3_camo","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M4A1 {"CUP_arifle_M4A1_camo","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M4A1SOPMOD {"CUP_arifle_M4A1_SOMMOD_green","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_MK12 {"CUP_srifle_Mk12SPR","CUP_muzzle_snds_Mk12","ACE_DBAL_A3_Green","optic_Holosight_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_MK16CQC {"CUP_arifle_Mk16_CQC_AFG_woodland","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Holosight_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_MK16 {"CUP_arifle_Mk16_STD_AFG_woodland","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_Holosight_lush_F",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_MK18 {"CUP_arifle_mk18_black","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_M4SBR {"CUP_arifle_SBR_od","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_SPAR {"arifle_SPAR_01_khk_F","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_ACR {"CUP_arifle_ACR_wdl_556","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_ACRC {"CUP_arifle_ACRC_wdl_556","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_BREN {"CUP_CZ_BREN2_556_8_Grn","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_TRG20 {"arifle_TRG20_F","muzzle_snds_M","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	#define TG_PRIMARY_TRG21 {"arifle_TRG21_F","muzzle_snds_M","ACE_DBAL_A3_Green","CUP_optic_MicroT1_OD",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""}
	
	
	class B_T_Soldier_SL_F {
		class HK416 {
			displayName = "squadlead";

			primaryWeapon[] = TG_SL_PRIMARY_HK416;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
			binocular = "CUP_SOFLAM";

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST};
			backpackClass[] = {TG_BACKPACK_RADIO};

			linkedItems[] = {LINKED_ITEMS_LDR};

			uniformItems[] = {UNIFORM_STANDARD}; 
			vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
			backpackItems[] = {{"CUP_HandGrenade_M67",4,1},{"SmokeShell",6,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"B_IR_Grenade",4,1} ,{"Laserbatteries",4,1}};

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {};
		};
		class HK416CQB: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_SL_PRIMARY_BREN;
		};
		
	};

	class B_T_soldier_exp_F {
		class HK416 {
			// Requires the following DLC:
			// Contact Platform
			displayName = "explosive specialist";

			primaryWeapon[] = TG_PRIMARY_HK416;
			secondaryWeapon[] = {"CUP_launch_M136_Loaded","","","",{"CUP_M136_M",1},{},""};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
			binocular = "Rangefinder";

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST};
			backpackClass[] = {TG_BACKPACK_LARGE};

			linkedItems[] = {LINKED_ITEMS_SUB};

			uniformItems[] = {UNIFORM_STANDARD}; 
			vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
			backpackItems[] = {EXP_SPEC_BACKPACK}; 

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {};
		};
		class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
	};

	class B_T_medic_F {
		class HK416 {
			// Requires the following DLC:
			// Contact Platform
			displayName = "CLS";

			primaryWeapon[] = TG_PRIMARY_HK416;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
			binocular = "Rangefinder";

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST};
			backpackClass[] = {TG_BACKPACK_LARGE};

			linkedItems[] = {LINKED_ITEMS_SUB};

			uniformItems[] = {UNIFORM_STANDARD_CLS}; 
			vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD",8,30},{"hlc_15Rnd_9x19_B_P226",2,15},{"SmokeShell",2,1}};
			backpackItems[] = {{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiBlue",4,1},{"CUP_HandGrenade_M67",2,1},{"B_IR_Grenade",2,1}};

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {{"ACE_bloodIV",8},{"ACE_bloodIV_500",8},{"ACE_fieldDressing",35},{"ACE_tourniquet",8},{"ACE_splint",8},{"ACE_morphine",15},{"ACE_epinephrine",20},{"ACE_personalAidKit",1}};
		};
		class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
	};

	class B_T_Soldier_TL_F {
		class HK416 {
			// Requires the following DLC:
			// Western Sahara
			// Contact Platform
			displayName = "teamleader";

			primaryWeapon[] = TG_PRIMARY_HK416;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
			binocular = "Laserdesignator_01_khk_F";

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST};
			backpackClass[] = {TG_BACKPACK_SMALL};

			linkedItems[] = {LINKED_ITEMS_LDR};

			uniformItems[] = {UNIFORM_STANDARD}; 
			vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
			backpackItems[] = {{"CUP_HandGrenade_M67",4,1},{"SmokeShell",6,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"B_IR_Grenade",4,1},{"Laserbatteries",4,1}};

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {};
		};
		class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
	};

	class B_T_Soldier_GL_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "B_Soldier_GL_F";

		primaryWeapon[] = {"sgun_aa40_snake_lxWS","muzzle_snds_12Gauge_snake_lxWS","ACE_SPIR","optic_r1_low_snake_lxWS",{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",20},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"vtf_MK13","","","",{"1Rnd_HE_Grenade_shell",1},{},""};

		uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
		headgearClass[] = {TG_HEADGEAR_A};
		facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
		vestClass[] = {TG_VEST_GL};
		backpackClass[] = {TG_BACKPACK_XLARGE};

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {UNIFORM_STANDARD_ALT}; 
		vestItems[] = {{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",2,20}};
		backpackItems[] = {{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",5,20},{"1Rnd_HE_Grenade_shell",6,1},{"1Rnd_Smoke_Grenade_shell",6,1},{"UGL_FlareWhite_F",4,1},{"SmokeShell",4,1},{"CUP_HandGrenade_M67",2,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	
	class B_T_soldier_AR_F {
		class M27 {
			// Requires the following DLC:
			// Contact Platform
			displayName = "B_soldier_AR_F";

			primaryWeapon[] = TG_AR_PRIMARY_M27;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST_AR};
			backpackClass[] = {TG_BACKPACK_SMALL};

			linkedItems[] = {LINKED_ITEMS_SUB};

			uniformItems[] = {UNIFORM_STANDARD_ALT}; 
			vestItems[] = {{"SmokeShell",4,1},{"CUP_HandGrenade_M67",4,1},{"B_IR_Grenade",4,1}};
			backpackItems[] = {{"CUP_100Rnd_556x45_BetaCMag_ar15",5,100},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1}, {"hlc_15Rnd_9x19_B_P226",4,15}};

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {};
		};
		class M27GRIP: M27 {
			primaryWeapon[] = TG_AR_PRIMARY_M27GRIP;
		};
		class SPAR: M27 {
			primaryWeapon[] = TG_AR_PRIMARY_SPARS;
		};
		class MK16SV: M27 {
			primaryWeapon[] = TG_AR_PRIMARY_MK16SV;
		};
	};
	
	
	class B_Soldier_SL_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "squadlead";

		primaryWeapon[] = {"CUP_CZ_BREN2_556_14_Grn","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "CUP_SOFLAM";

		uniformClass = "CUP_U_CRYE_G3C_AOR1";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_communicationsbelt_rngr";
		backpackClass[] = {TG_BACKPACK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LDR};

		uniformItems[] = {UNIFORM_STANDARD};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
		backpackItems[] = {{"CUP_HandGrenade_M67",4,1},{"SmokeShell",6,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"B_IR_Grenade",4,1} ,{"Laserbatteries",4,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_soldier_exp_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "explosive specialist";

		primaryWeapon[] = {"CUP_arifle_SBR_od","CUP_muzzle_snds_M16_camo","ACE_DBAL_A3_Green","optic_r1_high_khaki_lxWS",{"CUP_30Rnd_556x45_PMAG_OD_PULL",30},{},""};
		secondaryWeapon[] = {"CUP_launch_M136_Loaded","","","",{"CUP_M136_M",1},{},""};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Rangefinder";

		uniformClass = "CUP_U_CRYE_G3C_M81";
		headgearClass = "CUP_H_OpsCore_Spray_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_Fastbelt_rngr";
		backpackClass = "tmtm_b_predator_black";

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {UNIFORM_STANDARD};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
		backpackItems[] = {EXP_SPEC_BACKPACK}; 

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_medic_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CLS";

		primaryWeapon[] = {"CUP_CZ_BREN2_556_8_Grn","CUP_muzzle_snds_G36_hex","ACE_DBAL_A3_Green","CUP_optic_MicroT1_low_OD",{"CUP_30Rnd_556x45_PMAG_OD",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Rangefinder";

		uniformClass = "CUP_U_CRYEG3_V1";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_medical_mc";
		backpackClass = "B_Kitbag_sgg";

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {UNIFORM_STANDARD_CLS};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD",8,30},{"hlc_15Rnd_9x19_B_P226",2,15},{"SmokeShell",2,1}};
		backpackItems[] = {{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiBlue",4,1},{"CUP_HandGrenade_M67",2,1},{"B_IR_Grenade",2,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_bloodIV",8},{"ACE_bloodIV_500",8},{"ACE_fieldDressing",35},{"ACE_tourniquet",8},{"ACE_splint",8},{"ACE_morphine",15},{"ACE_epinephrine",20},{"ACE_personalAidKit",1}};
	};

	class B_Soldier_TL_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "teamleader";

		primaryWeapon[] = {"CUP_CZ_BREN2_556_14","suppressor_l_arid_lxWS","ACE_DBAL_A3_Green","optic_r1_high_arid_lxWS",{"CUP_30Rnd_556x45_PMAG_BLACK",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Laserdesignator_01_khk_F";

		uniformClass = "CUP_U_CRYE_G3C_MC_V2";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_tlbelt_rngr";
		backpackClass = "B_Kitbag_mcamo";

		linkedItems[] = {LINKED_ITEMS_LDR};

		uniformItems[] = {UNIFORM_STANDARD};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
		backpackItems[] = {{"CUP_HandGrenade_M67",4,1},{"SmokeShell",6,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"B_IR_Grenade",4,1} ,{"Laserbatteries",4,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_Soldier_GL_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "B_Soldier_GL_F";

		primaryWeapon[] = {"sgun_aa40_snake_lxWS","muzzle_snds_12Gauge_snake_lxWS","ACE_SPIR","optic_r1_low_snake_lxWS",{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",20},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"vtf_MK13","","","",{"1Rnd_HE_Grenade_shell",1},{},""};

		uniformClass = "CUP_U_CRYE_G3C_M81_Tan";
		headgearClass = "CUP_H_OpsCore_Covered_MTP_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_medicalbelt_mc";
		backpackClass = "tmtm_b_predator_MTPnoflag";

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {UNIFORM_STANDARD_ALT};
		vestItems[] = {{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",2,20}};
		backpackItems[] = {{"20Rnd_12Gauge_AA40_Slug_Snake_lxWS",5,20},{"1Rnd_HE_Grenade_shell",6,1},{"1Rnd_Smoke_Grenade_shell",6,1},{"UGL_FlareWhite_F",4,1},{"SmokeShell",4,1},{"CUP_HandGrenade_M67",2,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_soldier_AR_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "B_soldier_AR_F";

		primaryWeapon[] = {"CUP_arifle_HK_M27_VFG","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","CUP_optic_CompM2_Black",{"CUP_100Rnd_556x45_BetaCMag_ar15",100},{},"CUP_bipod_Harris_1A2_L_BLK"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};

		uniformClass = "CUP_U_CRYE_G3C_Tan_MC_US";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_US";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_medicalbelt_rngr";
		backpackClass = "tmtm_b_motherlode_MTPnoflag";

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {UNIFORM_STANDARD_ALT};
		vestItems[] = {{"SmokeShell",4,1},{"CUP_HandGrenade_M67",4,1},{"B_IR_Grenade",4,1}};
		backpackItems[] = {{"CUP_100Rnd_556x45_BetaCMag_ar15",5,100},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1}, {"hlc_15Rnd_9x19_B_P226",4,15}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_soldier_M_F {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "B_soldier_M_F";

		primaryWeapon[] = {"CUP_srifle_M2010_wdl","muzzle_snds_B_khk_F","acc_pointer_IR_lush_lxWS","optic_Nightstalker",{"CUP_5Rnd_762x67_M2010_M",5},{},"bipod_01_F_khk"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"CUP_hgun_MP7_woodland","CUP_muzzle_snds_MP7","ACE_DBAL_A3_Green","CUP_optic_MicroT1_low_OD",{"CUP_40Rnd_46x30_MP7",40},{},""};
		binocular = "Rangefinder";

		uniformClass = "CUP_U_CRYE_G3C_RGR";
		headgearClass = "CUP_H_OpsCore_Spray_US_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_lightbelt_rngr";
		backpackClass[] = {TG_BACKPACK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LDR};

		uniformItems[] = {UNIFORM_STANDARD_ALT};
		vestItems[] = {{"ItemcTabHCam",1},{"CUP_5Rnd_762x67_M2010_M",10,5}};
		backpackItems[] = {{"ACE_Kestrel4500",1},{"ACE_RangeCard",1},{"ACE_Tripod",1},{"SmokeShell",4,1},{"CUP_HandGrenade_M67",2,1},{"B_IR_Grenade",2,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"CUP_40Rnd_46x30_MP7",6,40}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_Soldier_lite_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "spotter";

		primaryWeapon[] = {"CUP_CZ_BREN2_556_8_Grn","CUP_muzzle_snds_G36_hex","ACE_DBAL_A3_Green","CUP_optic_MicroT1_low_OD",{"CUP_30Rnd_556x45_PMAG_OD",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Rangefinder";

		uniformClass = "CUP_U_CRYEG3_V2";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_Fastbelt_mc";
		backpackClass = "B_AssaultPack_khk";

		linkedItems[] = {LINKED_ITEMS_SUB};

		uniformItems[] = {{"hlc_15Rnd_9x19_B_P226",2,15}, {"ItemcTabHCam",1}, {"ACE_IR_Strobe_Item",1}};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD",8,30},{"hlc_15Rnd_9x19_B_P226",2,15} , {"ACE_CableTie",2},{"ACE_CTS9",2,1}};
		backpackItems[] = {{"ACE_RangeCard",1},{"ACE_Tripod",1},{"SmokeShell",4,1},{"CUP_HandGrenade_M67",4,1},{"B_IR_Grenade",2,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_officer_F {
		// Requires the following DLC:
		// Contact Platform
		displayName = "commander";

		primaryWeapon[] = {"CUP_CZ_BREN2_556_8","CUP_muzzle_snds_G36_black","ACE_DBAL_A3_Green","optic_MRCO",{"CUP_30Rnd_556x45_PMAG_BLACK_PULL",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Rangefinder";
		
		uniformClass = "CUP_U_CRYE_G3C_MC_V3";
		headgearClass = "CUP_H_OpsCore_Covered_MCAM_SF";
		facewearClass[] = {BLU_FACEWEAR}; 
		vestClass = "CUP_V_CPC_communicationsbelt_coy";
		backpackClass[] = {TG_BACKPACK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_CDR}; 

		uniformItems[] = {{"ACE_IR_Strobe_Item",1}};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_BLACK_PULL",8,30},{"hlc_15Rnd_9x19_B_P226",4,15}};
		backpackItems[] = {{"CUP_HandGrenade_M67",4,1},{"SmokeShell",6,1},{"ACE_Chemlight_HiBlue",4,1},{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiWhite",4,1},{"B_IR_Grenade",4,1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class B_soldier_UAV_F {
		class HK416 {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "drone operator";

		primaryWeapon[] = TG_PRIMARY_HK416; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"vtf_MK13","","","",{"1Rnd_RC40_HE_shell_RF",1},{},""};

		uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
		headgearClass[] = {TG_HEADGEAR_A};
		facewearClass[] = {TG_FACEWEAR_A}; 
		vestClass[] = {TG_VEST};
		backpackClass[] = {TG_BACKPACK_RADIO}; 

		linkedItems[] = {"ItemMap","B_UavTerminal","TFAR_anprc152","ItemCompass","ItemWatch","ACE_NVG_Wide_Black_WP"};

		uniformItems[] = {{"ItemcTabHCam",1}, {"ACE_IR_Strobe_Item",1},{"hlc_15Rnd_9x19_B_P226",2,15}};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_BLACK_PULL",8,30}, {"CUP_HandGrenade_M67",2,1},{"SmokeShell",2,1}};
		backpackItems[] = {{"ACE_UAVBattery",2},{"ToolKit",1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
}; 

	class B_Soldier_F {
		class HK416{ 
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "driver";

		primaryWeapon[] = TG_PRIMARY_HK416; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
		binocular = "Laserdesignator_01_khk_F";

		uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
		headgearClass[] = {TG_HEADGEAR_A};
		facewearClass[] = {TG_FACEWEAR_A}; 
		vestClass[] = {TG_VEST};
		backpackClass[] = {TG_BACKPACK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_CDR}; 

		uniformItems[] = {{"ItemcTabHCam",1}, {"ACE_IR_Strobe_Item",1}};
		vestItems[] = {{"CUP_30Rnd_556x45_PMAG_BLACK_PULL",8,30}, {"hlc_15Rnd_9x19_B_P226",4,15}};
		backpackItems[] = {{"SmokeShell",4,1},{"CUP_HandGrenade_M67",4,1}, {"ToolKit",1}};

		basicMedUniform[] = {BLU_MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
}; 

class B_recon_medic_F {
		class HK416 {
			// Requires the following DLC:
			// Contact Platform
			displayName = "command medic";

			primaryWeapon[] = TG_PRIMARY_HK416;
			secondaryWeapon[] = {};
			handgunWeapon[] = {"hlc_pistol_P229R_Combat","muzzle_snds_L","CUP_acc_CZ_M3X","HLC_Optic228_Docter_CADEX",{"hlc_15Rnd_9x19_B_P226",15},{},""};
			binocular = "Rangefinder";

			uniformClass[] = {TG_UNIFORM_1, TG_UNIFORM_2, TG_UNIFORM_3};
			headgearClass[] = {TG_HEADGEAR_A};
			facewearClass[] = {TG_FACEWEAR_A, TG_CAMO_NETTING}; 
			vestClass[] = {TG_VEST};
			backpackClass[] = {TG_BACKPACK_RADIO};

			linkedItems[] = {LINKED_ITEMS_CDR};

			uniformItems[] = {UNIFORM_STANDARD_CLS}; 
			vestItems[] = {{"CUP_30Rnd_556x45_PMAG_OD",8,30},{"hlc_15Rnd_9x19_B_P226",2,15},{"SmokeShell",2,1}};
			backpackItems[] = {{"ACE_Chemlight_HiGreen",4,1},{"ACE_Chemlight_HiRed",4,1},{"ACE_Chemlight_HiBlue",4,1},{"CUP_HandGrenade_M67",2,1},{"B_IR_Grenade",2,1}};

			basicMedUniform[] = {BLU_MEDICAL_STANDARD};
			basicMedVest[] = {};
			basicMedBackpack[] = {{"ACE_bloodIV",8},{"ACE_bloodIV_500",8},{"ACE_fieldDressing",35},{"ACE_tourniquet",8},{"ACE_splint",8},{"ACE_morphine",15},{"ACE_epinephrine",20},{"ACE_personalAidKit",1}};
		};
		class HK416CQB: HK416 {
			primaryWeapon[] = TG_PRIMARY_HK416CQB;
		};
		class M16A4GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4GRIP;
		};
		class M16A4: HK416 {
			primaryWeapon[] = TG_PRIMARY_M16A4;
		};
		class M27GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27GRIP;
		};
		class M27: HK416 {
			primaryWeapon[] = TG_PRIMARY_M27;
		};
		class M4A1GRIP: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1GRIP;
		};
		class M4A1: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1;
		};
		class M4A1SOPMOD: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4A1SOPMOD;
		};
		class MK12: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK12;
		};
		class MK16CQC: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16CQC;
		};
		class MK16: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK16;
		};
		class MK18: HK416 {
			primaryWeapon[] = TG_PRIMARY_MK18;
		};
		class M4SBR: HK416 {
			primaryWeapon[] = TG_PRIMARY_M4SBR;
		};
		class SPAR: HK416 {
			primaryWeapon[] = TG_PRIMARY_SPAR;
		};
		class ACR: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACR;
		};
		class ACRC: HK416 {
			primaryWeapon[] = TG_PRIMARY_ACRC;
		};
		class BREN: HK416 {
			primaryWeapon[] = TG_PRIMARY_BREN;
		};
		class TRG20: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG20;
		};
		class TRG21: HK416 {
			primaryWeapon[] = TG_PRIMARY_TRG21;
		};
	};

};

