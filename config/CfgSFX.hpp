class CfgSFX
{
    class FR_Jammer_AmbientLoop
    {
        sound0[] = {"media\sfx\jammer_ambientLoop.ogg", 2, 1, 25, 1, 0, 0, 0};
        sounds[] = {sound0};
        empty[] = {"", 0, 0, 0, 0, 0, 0, 0};
    };
};

class CfgVehicles
{
    class FR_Jammer_AmbientLoopSound
    {
        sound = "FR_Jammer_AmbientLoop";
    };
};