// XPTCuratorLoadouts.hpp
// Used for defining advanced loadouts for Zeus spawned units (works almost the same as XPTLoadouts)
// Use XPT_fnc_exportInventory to get correct loadout format, picture guide can be found here: https://imgur.com/a/GrDaJNZ, courtesy of O'Mally
// Supports applying loadouts to crew in vehicles as long as crew classname matches the one defined in here
// Default behaviour is to check if the Zeus placed unit has a special loadout defined. Otherwise, it will use the default loadout as normal
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class curatorLoadouts
{
	// Conscript Loadouts

	#define CON_HEADGEAR "tmtm_h_k6_ranger", "tmtm_h_k6_olive", "tmtm_h_k6_teal"
	#define CON_HEADGEAR_M "CUP_H_RUS_Ratnik_Balaclava_6M21_A_Tacs_1", "CUP_H_RUS_Ratnik_Balaclava_6M21_A_Tacs_2", "CUP_H_RUS_Ratnik_Balaclava_6M21_A_Tacs_3", "CUP_H_RUS_Ratnik_Balaclava_6M21_A_Tacs_4", "CUP_H_RUS_Ratnik_Balaclava_6M21_EMR_Summer_1", "CUP_H_RUS_Ratnik_Balaclava_6M21_EMR_Summer_2", "CUP_H_RUS_Ratnik_Balaclava_6M21_Green_1", "CUP_H_RUS_Ratnik_Balaclava_6M21_Green_2", "CUP_H_RUS_Ratnik_Balaclava_6M21_Green_3", "CUP_H_RUS_Ratnik_Balaclava_6M21_Green_4", "CUP_H_RUS_Ratnik_Balaclava_6M21_Olive_1", "CUP_H_RUS_Ratnik_Balaclava_6M21_Olive_2", "CUP_H_RUS_Ratnik_Balaclava_6M21_Winter_Green_1", "CUP_H_RUS_Ratnik_Balaclava_6M21_Winter_Green_2"
	#define CON_FACEWEAR "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_1", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_2", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_3", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_4", "CUP_G_RUS_Ratnik_Balaclava_Green_1", "CUP_G_RUS_Ratnik_Balaclava_Green_2", "CUP_G_RUS_Ratnik_Balaclava_Green_3", "CUP_G_RUS_Ratnik_Balaclava_Green_4", "CUP_G_RUS_Ratnik_Balaclava_Olive_1", "CUP_G_RUS_Ratnik_Balaclava_Olive_2", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_1", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_2"
	#define CON_FACEWEAR_M ""
	#define CON_UNIFORM "tmtm_u_maskhalat_berezkaWhite", "tmtm_u_maskhalat_berezkaWhite_loose", "tmtm_u_maskhalat_berezkaYellow", "tmtm_u_maskhalat_berezkaYellow_loose", "tmtm_u_maskhalat_svitanok_loose"
	#define CON_VEST "CUP_V_B_RRV_TL", "CUP_V_B_RRV_Scout3_GRN", "CUP_V_B_RRV_Scout", "CUP_V_B_RRV_Medic", "CUP_V_B_RRV_Officer", "CUP_V_B_RRV_TL_CB", "CUP_V_B_RRV_Scout3", "CUP_V_B_RRV_Scout_CB", "CUP_V_B_RRV_Medic_CB", "CUP_V_B_RRV_Officer_CB"
	#define CON_VEST_AR "CUP_V_B_RRV_MG_GRN", "CUP_V_B_RRV_MG"
	#define CON_VEST_AT "CUP_V_O_RUS_RPS_Smersh_AK_Brown", "CUP_V_O_RUS_RPS_Smersh_AK_Green", "CUP_V_O_RUS_RPS_Smersh_AK_ModernOlive"
	#define CON_VEST_GL "CUP_V_B_RRV_Scout2", "CUP_V_B_RRV_Scout2_CB"
	#define CON_VEST_M "CUP_V_B_RRV_Light", "CUP_V_B_RRV_Light_CB"
	#define CON_BACKPACK "tmtm_b_rpsSmersh_ak_brown", "tmtm_b_rpsSmersh_ak_green", "tmtm_b_rpsSmersh_ak_olive"
	#define CON_BACKPACK_AR "tmtm_b_rpsSmersh_pkp_brown", "tmtm_b_rpsSmersh_pkp_green", "tmtm_b_rpsSmersh_pkp_olive"
	#define CON_BACKPACK_AT "CUP_B_RPGPack_Khaki"
	#define CON_BACKPACK_GL "tmtm_b_rpsSmersh_vogSPP_brown", "tmtm_b_rpsSmersh_vogSPP_green", "tmtm_b_rpsSmersh_vogSPP_olive"
	#define CON_BACKPACK_M "tmtm_b_rpsSmersh_svd_brown", "tmtm_b_rpsSmersh_svd_green", "tmtm_b_rpsSmersh_svd_olive"


	class CUP_O_INS_Commander {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Commander";

		primaryWeapon[] = {"CUP_arifle_AKS74U_railed","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST};
		backpackClass[] = {CON_BACKPACK};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",8,30}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class CUP_O_INS_Officer: CUP_O_INS_Commander {};

	class CUP_O_INS_Soldier {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier";

		primaryWeapon[] = {"CUP_arifle_AK12_GP34_lush","","CUP_acc_Flashlight","CUP_optic_1P87_RIS_woodland",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{"CUP_1Rnd_HE_GP25_M",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST_GL};
		backpackClass[] = {CON_BACKPACK};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_IlumFlareRed_GP25_M",4,1}};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_1Rnd_HE_GP25_M",7,1}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_AT {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AT";

		primaryWeapon[] = {"CUP_arifle_AK12_VG_bicolor","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{},""};
		secondaryWeapon[] = {"launch_RPG7_F","","","",{"CUP_OG7_M",1},{},""};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST_AT};
		backpackClass[] = {CON_BACKPACK_AT};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",8,30}};
		backpackItems[] = {{"CUP_OG7_M",3,1},{"CUP_PG7V_M",1,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_AR {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AR";

		primaryWeapon[] = {"CUP_lmg_PKM","CUP_muzzle_mfsup_Flashhider_PK_Black","","",{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Red_M",100},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST_AR};
		backpackClass[] = {CON_BACKPACK_AR};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",2,1},{"SmokeShell",2,1},{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M",2,100}};
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M",3,100}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_MG {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_MG";

		primaryWeapon[] = {"CUP_lmg_PKM_top_rail","CUP_muzzle_mfsup_Flashhider_PK_Black","","optic_NVS",{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M",100},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST_AR};
		backpackClass[] = {CON_BACKPACK_AR};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",2,1},{"SmokeShell",2,1},{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M",2,100}};
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Green_M",3,100}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_GL {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_GL";

		primaryWeapon[] = {"CUP_arifle_AK12_GP34_lush","","CUP_acc_Flashlight","CUP_optic_1P87_RIS_woodland",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{"CUP_1Rnd_HE_GP25_M",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST_GL};
		backpackClass[] = {CON_BACKPACK};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {{"CUP_IlumFlareRed_GP25_M",4,1}};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_1Rnd_HE_GP25_M",7,1}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_AK74 {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AK74";

		primaryWeapon[] = {"CUP_arifle_AK74M","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_ekp_8_02",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST};
		backpackClass[] = {CON_BACKPACK};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class CUP_O_INS_Soldier_Ammo: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Story_Lopotev: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Crew: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_Engineer: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Story_Bardak: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Medic: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Saboteur: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_Exp: CUP_O_INS_Soldier_AK74 {};
	class CUP_O_INS_Soldier_LAT: CUP_O_INS_Soldier_AK74 {
		secondaryWeapon[] = {"CUP_launch_RPG18_Loaded","","","",{"CUP_RPG18_M",1},{},""};
	};

	class CUP_O_INS_Sniper {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Sniper";

		primaryWeapon[] = {"CUP_srifle_SVD","","","CUP_optic_GOSHAWK",{"ACE_10Rnd_762x54_Tracer_mag",10},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR_M};
		facewearClass[] = {CON_FACEWEA_M};
		vestClass[] = {CON_VEST_M};
		backpackClass[] = {CON_BACKPACK_M};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",1,1}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_AA {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AA";

		primaryWeapon[] = {"CUP_arifle_AK74M","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_ekp_8_02",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR};
		facewearClass[] = {CON_FACEWEAR};
		vestClass[] = {CON_VEST};
		backpackClass[] = {CON_BACKPACK};

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Pilot {
		// Requires the following DLC:
		// Contact Platform
		displayName = "CUP_O_INS_Pilot";

		primaryWeapon[] = {"CUP_arifle_AKS74U_railed","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"vtf_MK13_black","","","",{"1Rnd_RC40_HE_shell_RF",1},{},""};
		binocular = "Binocular";

		uniformClass[] = {CON_UNIFORM};
		headgearClass[] = {CON_HEADGEAR_M};
		facewearClass[] = {CON_FACEWEAR_M};
		vestClass[] = {CON_VEST_AT};
		backpackClass = "O_UAV_01_backpack_F";

		linkedItems[] = {"ItemMap","O_UavTerminal","","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30},{"HandGrenade",4,1},{"SmokeShell",2,1},{"1Rnd_RC40_HE_shell_RF",1,1}};
		backpackItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};


	/* Unused Loadouts
	class CUP_O_INS_Commander {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Commander";

		primaryWeapon[] = {"CUP_arifle_AKS74U_railed","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_4_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_AK_Green";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30},{"HandGrenade",4,1},{"SmokeShell",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
	};

	class CUP_O_INS_Soldier_AR {
		// Requires the following DLC:
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AR";

		primaryWeapon[] = {"CUP_lmg_PKM","CUP_muzzle_mfsup_Flashhider_PK_Black","","",{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Red_M",100},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "tmtm_u_maskhalat_berezkaYellow";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_PKP_Green";
		backpackClass = "B_FieldPack_green_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",2,1},{"SmokeShell",2,1},{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Red_M",2,100}};
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_762x54_PK_Tracer_Red_M",4,100}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_GL {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_GL";

		primaryWeapon[] = {"CUP_arifle_AK74M_GL_railed","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_1P87_RIS",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{"CUP_1Rnd_HE_GP25_M",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_3_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_VOG_SPP_Green";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30},{"CUP_1Rnd_HE_GP25_M",6,1},{"CUP_IlumFlareWhite_GP25_M",4,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
	};

	class CUP_O_INS_Soldier_AT {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AT";

		primaryWeapon[] = {"CUP_arifle_AK12_VG_bicolor","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_AC11704_Black",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{},""};
		secondaryWeapon[] = {"launch_RPG7_F","","","",{"CUP_OG7_M",1},{},""};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_1_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_AK_Green";
		backpackClass = "CUP_B_RPGPack_Khaki";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",8,30}};
		backpackItems[] = {{"CUP_OG7_M",3,1},{"CUP_PG7V_M",1,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};

	class CUP_O_INS_Soldier_AK74 {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AK74";

		primaryWeapon[] = {"CUP_arifle_AK74M","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_ekp_8_02",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_4_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_AK_Green";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
	};

	class CUP_O_INS_Soldier {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier";

		primaryWeapon[] = {"CUP_arifle_AK12_GP34_lush","","CUP_acc_Flashlight","CUP_optic_1P87_RIS_woodland",{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_M",30},{"CUP_1Rnd_HE_GP25_M",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_4_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_VOG_SPP_Green";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_IlumFlareRed_GP25_M",4,1}};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Green_Tracer_545x39_AK12_Green_M",8,30},{"CUP_1Rnd_HE_GP25_M",8,1},{"CUP_IlumFlareRed_GP25_M",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
	};

	class CUP_O_INS_Soldier_AA {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		// Contact Platform
		displayName = "CUP_O_INS_Soldier_AK74";

		primaryWeapon[] = {"CUP_arifle_AK74M","CUP_muzzle_mfsup_Flashhider_545x39_Black","CUP_acc_Flashlight","CUP_optic_ekp_8_02",{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {};

		uniformClass = "U_I_C_Soldier_Para_4_F";
		headgearClass = "lxWS_H_PASGT_goggles_olive_F";
		facewearClass = "CUP_G_RUS_Ratnik_Balaclava_Green_3";
		vestClass = "CUP_V_O_RUS_RPS_Smersh_AK_Green";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","CUP_NVG_1PN138"};

		uniformItems[] = {};
		vestItems[] = {{"HandGrenade",4,1},{"SmokeShell",2,1},{"CUP_30Rnd_TE1_Red_Tracer_545x39_AK74M_M",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",1},{"ACE_morphine",1},{"ACE_tourniquet",1}};
		basicMedVest[] = {};
	};
	*/

	//Spetznaz loadouts 
	#define SP_HEADGEAR "tmtm_h_fastHeadset_atacsFgGrey", "tmtm_h_fastHeadset_atacsFgOlive", "CUP_H_RUS_6B47_NV_6M2_Summer", "CUP_H_RUS_6B47_NV_6M21_Summer", "H_Simc_pasgt_m81_scrim_panama_nv", "tmtm_h_fastHeadset_multicamOlive", "H_HelmetAggressor_sb_taiga_RF", "H_HelmetSpecB_paint1", "H_HelmetSpecB_snakeskin", "H_HelmetSpecB_paint2", "tmtm_h_k6_emr", "tmtm_h_k6_ranger", "tmtm_h_helmetLight_olive", "H_HelmetB_light_desert", "H_HelmetB_light_grass", "H_HelmetB_light_snakeskin", "tmtm_h_ACHHeadset_atacsFg", "tmtm_h_ACHGogglesHeadset_atacsFg", "tmtm_h_ACHHeadset_emr", "tmtm_h_ACHGogglesHeadset_emr", "CUP_H_CZ_Helmet07", "CUP_H_CZ_Helmet10", "CUP_H_OpsCore_Green_SF", "CUP_H_OpsCore_Grey_SF", "CUP_H_OpsCore_Covered_MCAM_SF", "CUP_H_OpsCore_Spray_SF"
	#define SP_FACEWEAR "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_1", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_2", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_3", "CUP_G_RUS_Ratnik_Balaclava_A_Tacs_4", "CUP_G_RUS_Ratnik_Balaclava_EMR_Autumn_1", "CUP_G_RUS_Ratnik_Balaclava_EMR_Autumn_2", "CUP_G_RUS_Ratnik_Balaclava_EMR_Summer_1", "CUP_G_RUS_Ratnik_Balaclava_EMR_Summer_2", "CUP_G_RUS_Ratnik_Balaclava_Olive_1", "CUP_G_RUS_Ratnik_Balaclava_Olive_2", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_1", "CUP_G_RUS_Ratnik_Balaclava_Winter_Green_2", "G_Bandanna_beast", "G_Bandanna_oli", "G_Bandanna_Skull2", "G_Bandanna_Skull1", "G_Bandanna_khk"
	#define SP_UNIFORM "CUP_U_O_RUS_Soldier_Gorka_A_Tacs_2", "CUP_U_O_RUS_Soldier_Gorka_A_Tacs_3", "CUP_U_O_RUS_Soldier_Gorka_A_Tacs_4", "CUP_U_O_RUS_Soldier_Gorka_EMR_2", "CUP_U_O_RUS_Soldier_Gorka_EMR_3", "CUP_U_O_RUS_Soldier_Gorka_EMR_4", "CUP_U_O_RUS_Soldier_Gorka_Khaki_2", "CUP_U_O_RUS_Soldier_Gorka_Khaki_3", "CUP_U_O_RUS_Soldier_Gorka_Khaki_4", "CUP_U_O_RUS_Soldier_VKPO_Winter_2", "CUP_U_O_RUS_Soldier_VKPO_Winter_3", "CUP_U_O_RUS_Soldier_VKPO_Winter_4"
	#define SP_VEST_TL "CUP_V_JPC_tl_rngr", "CUP_V_JPC_communications_rngr", "CUP_V_JPC_tl_mc", "CUP_V_JPC_communications_mc", "CUP_V_CPC_tl_rngr", "CUP_V_CPC_communications_rngr", "CUP_V_CPC_tl_mc", "CUP_V_CPC_communications_mc"
	#define SP_VEST_GL "CUP_V_JPC_weapons_rngr", "CUP_V_JPC_weapons_mc", "CUP_V_CPC_weapons_rngr", "CUP_V_CPC_weapons_mc"
	#define SP_VEST_MED "CUP_V_JPC_medical_rngr", "CUP_V_JPC_Fast_rngr", "CUP_V_JPC_tl_rngr", "CUP_V_JPC_medical_mc", "CUP_V_JPC_Fast_mc", "CUP_V_JPC_tl_mc", "CUP_V_CPC_medical_rngr", "CUP_V_CPC_Fast_rngr", "CUP_V_CPC_tl_rngr", "CUP_V_CPC_medical_mc", "CUP_V_CPC_Fast_mc", "CUP_V_CPC_tl_mc"
	#define SP_VEST_M "CUP_V_B_JPC_OD_Light", "CUP_V_JPC_medical_rngr", "CUP_V_B_JPC_MCam_Light", "CUP_V_JPC_medical_mc"
	#define SP_BACKPACK "tmtm_b_rpsSmersh_ak_brown", "tmtm_b_rpsSmersh_ak_green", "tmtm_b_rpsSmersh_ak_olive"
	#define SP_BACKPACK_GL "tmtm_b_rpsSmersh_vogSPP_brown", "tmtm_b_rpsSmersh_vogSPP_green", "tmtm_b_rpsSmersh_vogSPP_olive"
	#define SP_BACKPACK_AR "tmtm_b_rpsSmersh_pkp_brown", "tmtm_b_rpsSmersh_pkp_green", "tmtm_b_rpsSmersh_pkp_olive"
	#define SP_BACKPACK_M "tmtm_b_rpsSmersh_svd_brown", "tmtm_b_rpsSmersh_svd_green", "tmtm_b_rpsSmersh_svd_olive"

	class O_R_Soldier_TL_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_TL_F";

		primaryWeapon[] = {"arifle_AK12_GL_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{"1Rnd_HE_Grenade_shell",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_TL};
		backpackClass[] = {SP_BACKPACK_GL};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30},{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1}};
		backpackItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",8,30},{"1Rnd_HE_Grenade_shell",5,1},{"1Rnd_Smoke_Grenade_shell",2,1},{"CUP_FlareWhite_M203",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_TL_F: O_R_Soldier_TL_F {};
	class O_R_recon_TL_F: O_R_Soldier_TL_F {};

	class O_R_Soldier_LAT_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_LAT_F";

		primaryWeapon[] = {"arifle_AK12_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{},""};
		secondaryWeapon[] = {"launch_RPG32_green_F","","","",{"RPG32_F",1},{},""};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_MED};
		backpackClass[] = {SP_BACKPACK};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30},{"MiniGrenade",4,1}};
		backpackItems[] = {{"RPG32_HE_F",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",6,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_LAT_F: O_R_Soldier_LAT_F {};
	class O_R_recon_LAT_F: O_R_Soldier_LAT_F {};

	class O_R_soldier_M_F {
		// Requires the following DLC:
		// Marksmen
		// Contact Platform
		displayName = "O_R_soldier_M_F";

		primaryWeapon[] = {"srifle_DMR_04_F","","ACE_DBAL_A3_Green","optic_Arco_blk_F",{"10Rnd_127x54_Mag",10},{},"bipod_02_F_arid"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_M};
		backpackClass[] = {SP_BACKPACK_M};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"10Rnd_127x54_Mag",3,10}};
		backpackItems[] = {{"10Rnd_127x54_Mag",10,10}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_M2_F: O_R_soldier_M_F {};
	class O_R_Patrol_Soldier_M_F: O_R_soldier_M_F {};
	class O_R_recon_M_F: O_R_soldier_M_F {};

	class O_R_JTAC_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_JTAC_F";

		primaryWeapon[] = {"arifle_AK12_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_TL};
		backpackClass[] = {SP_BACKPACK};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30}};
		backpackItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",8,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_A_F: O_R_JTAC_F {};
	class O_R_recon_JTAC_F: O_R_JTAC_F {};
	
	class O_R_Soldier_GL_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_GL_F";

		primaryWeapon[] = {"arifle_AK12_GL_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{"1Rnd_HE_Grenade_shell",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_GL};
		backpackClass[] = {SP_BACKPACK_GL};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30},{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1}};
		backpackItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",8,30},{"1Rnd_HE_Grenade_shell",5,1},{"1Rnd_Smoke_Grenade_shell",2,1},{"CUP_FlareWhite_M203",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_GL_F: O_R_Soldier_GL_F {};
	class O_R_recon_GL_F: O_R_Soldier_GL_F {};

	class O_R_soldier_exp_F {
		// Requires the following DLC:
		// Contact Platform
		// Western Sahara
		displayName = "O_R_soldier_exp_F";

		primaryWeapon[] = {"arifle_AK12U_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_MED};
		backpackClass[] = {SP_BACKPACK};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30}};
		backpackItems[] = {{"ACE_DefusalKit",1},{"ACE_M26_Clacker",1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",5,30},{"DemoCharge_Remote_Mag",4,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_Engineer_F: O_R_soldier_exp_F {};
	class O_R_recon_exp_F: O_R_soldier_exp_F {};

	class O_R_medic_F {
		// Requires the following DLC:
		// Contact Platform
		// Western Sahara
		displayName = "O_R_medic_F";

		primaryWeapon[] = {"arifle_AK12U_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_MED};
		backpackClass[] = {SP_BACKPACK};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",4,30}};
		backpackItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",6,30}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {{"ACE_fieldDressing",10},{"ACE_bloodIV",2},{"ACE_bloodIV_500",4},{"ACE_epinephrine",4},{"ACE_morphine",4},{"ACE_splint",4},{"ACE_tourniquet",4}};
	};
	class O_R_Patrol_Soldier_Medic: O_R_medic_F {};
	class O_R_recon_medic_F: O_R_medic_F {};

	class O_R_Soldier_AR_F {
		// Requires the following DLC:
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_AR_F";

		primaryWeapon[] = {"arifle_RPK12_lush_F","suppressor_h_lush_lxWS","ACE_DBAL_A3_Green","optic_Arco_AK_lush_F",{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",75},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass[] = {SP_UNIFORM};
		headgearClass[] = {SP_HEADGEAR};
		facewearClass[] = {SP_FACEWEAR};
		vestClass[] = {SP_VEST_MED};
		backpackClass[] = {SP_BACKPACK_AR};

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",2,75}};
		backpackItems[] = {{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",4,75}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class O_R_Patrol_Soldier_AR2_F: O_R_Soldier_AR_F {};
	class O_R_Patrol_Soldier_AR_F: O_R_Soldier_AR_F {};
	class O_R_recon_AR_F: O_R_Soldier_AR_F {};

	/* Unused Loadouts
	class O_R_Soldier_TL_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_TL_F";

		primaryWeapon[] = {"arifle_AK12_GL_lush_arco_pointer_F","suppressor_h_lush_lxWS","acc_pointer_IR","optic_Arco_AK_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{"1Rnd_HE_Grenade_shell",1},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_R_Gorka_01_F";
		headgearClass = "tmtm_h_k6_teal";
		facewearClass = "G_Balaclava_blk";
		vestClass = "V_SmershVest_01_radio_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",2,30}};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"1Rnd_HE_Grenade_shell",6,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",6,30},{"1Rnd_Smoke_Grenade_shell",2,1},{"CUP_FlareWhite_M203",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
	};

	class O_R_Soldier_AR_F {
		// Requires the following DLC:
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_AR_F";

		primaryWeapon[] = {"arifle_RPK12_lush_F","suppressor_h_lush_lxWS","acc_pointer_IR","optic_Arco_AK_lush_F",{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",75},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};

		uniformClass = "U_O_R_Gorka_01_F";
		headgearClass = "tmtm_h_k6_teal";
		facewearClass = "G_Balaclava_blk";
		vestClass = "V_SmershVest_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",1,75}};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",2,1},{"75rnd_762x39_AK12_Lush_Mag_Tracer_F",4,75}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
	};

	class O_R_soldier_M_F {
		// Requires the following DLC:
		// Marksmen
		// Contact Platform
		displayName = "O_R_soldier_M_F";

		primaryWeapon[] = {"srifle_DMR_04_F","","acc_pointer_IR","optic_Arco_blk_F",{"10Rnd_127x54_Mag",10},{},"bipod_02_F_arid"};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};

		uniformClass = "U_O_R_Gorka_01_F";
		headgearClass = "tmtm_h_k6_teal";
		facewearClass = "G_Balaclava_blk";
		vestClass = "V_SmershVest_01_radio_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {{"10Rnd_127x54_Mag",2,10}};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"MiniGrenade",2,1},{"SmokeShell",4,1},{"ACE_M84",2,1},{"10Rnd_127x54_Mag",8,10}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
	};

	class O_R_Soldier_LAT_F {
		// Requires the following DLC:
		// Apex
		// Contact Platform
		// Western Sahara
		displayName = "O_R_Soldier_LAT_F";

		primaryWeapon[] = {"arifle_AK12_lush_F","suppressor_h_lush_lxWS","acc_pointer_IR","optic_Holosight_lush_F",{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",30},{},""};
		secondaryWeapon[] = {"launch_RPG32_green_F","","","",{"RPG32_HE_F",1},{},""};
		handgunWeapon[] = {"hgun_Rook40_F","muzzle_snds_L","","",{"16Rnd_9x21_Mag",17},{},""};
		binocular = "Rangefinder";

		uniformClass = "U_O_R_Gorka_01_F";
		headgearClass = "tmtm_h_k6_teal";
		facewearClass = "G_Balaclava_blk";
		vestClass = "V_SmershVest_01_radio_F";
		backpackClass = "CUP_B_RPGPack_Khaki";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","O_NVGoggles_grn_F"};

		uniformItems[] = {{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",2,30}};
		vestItems[] = {{"16Rnd_9x21_Mag",2,17},{"SmokeShell",4,1},{"ACE_M84",2,1},{"30rnd_762x39_AK12_Lush_Mag_Tracer_F",6,30},{"HandGrenade",4,1}};
		backpackItems[] = {{"RPG32_HE_F",2,1}};

		basicMedUniform[] = {{"ACE_fieldDressing",6},{"ACE_epinephrine",2},{"ACE_morphine",2},{"ACE_tourniquet",2},{"ACE_splint",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	*/
};