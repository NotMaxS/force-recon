// initServer.sqf
// Executes only on the server at mission start
// No parameters are passed to this script

// Call the template initServer function
[] call XPT_fnc_initServer; // DO NOT CHANGE THIS LINE

// Call the script to handle initial task setup
[] execVM "scripts\tasks.sqf";

//////////////////////////////////////////////////////////
///// Add any mission specific code after this point /////
//////////////////////////////////////////////////////////

// Create a list of mission objects that should not be curator editable
XPT_blacklistedMissionObjects = [];

// deform terrain
// ONLY CALL ONCE AND ONLY ON SERVER
[] call TG_fnc_terrainDeform;

// Server will parse all markers in global channel on mission load and move them to editor layer
// Global channel is disabled on client, hence markers would be greyed out otherwise
["FR_PlayerLoadedEvent", 
    {
        // Respond once, then ignore any further events
        [_thisType, _thisId] call CBA_fnc_removeEventHandler;
        
        [0, -1] call FR_fnc_markers_bulkMoveLayer;
    }
] call CBA_fnc_addEventHandlerArgs;

//remove the HVT unit from garbage collector so that his inventory item does not disappear on death 
//would not be end of the world if this doesnt work but creates more work for zeus 
removeFromRemainsCollector [hvtdude];  

//time multiplier
//setTimeMultiplier 0.1;

//Event handler for counter battery fnc 
[
    "SLV_PlayerArtilleryFired", 
    {_this call SLV_fnc_counterBattery;}
] call CBA_fnc_addEventHandler; 

//Event handler to handle end mission logic fnc
[
    "FR_MissionEndEvent", 
    {[] call SLV_fnc_endMissionLogic;}
] call CBA_fnc_addEventHandler; 

//Event handler to handle tieing tasks into more streamlined end mission logic fnc
[
    "FR_MissionTaskCompletedEvent", 
    {_this call FR_fnc_mission_onTaskCompleted;}
] call CBA_fnc_addEventHandler;
